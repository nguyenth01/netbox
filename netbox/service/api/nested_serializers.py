from rest_framework import serializers

from dcim.api.nested_serializers import NestedInstanceSerializer
from service.constants import *
from service.models import ServiceGroup, Service, ModuleGroup, ServiceUser
from utilities.api import WritableNestedSerializer, ChoiceField


class NestedServiceGroupSerializer(WritableNestedSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='service-api:service-group-detail')

    class Meta:
        model = ServiceGroup
        fields = ['url', 'id', 'code', 'name', 'status', 'slug']


class NestedServiceSerializer(WritableNestedSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='service-api:service-detail')
    status = ChoiceField(choices=SERVICE_STATUS_CHOICES)

    class Meta:
        model = Service
        fields = ['url', 'id', 'code', 'name', 'status', 'slug']


class NestedModuleGroupSerializer(WritableNestedSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='service-api:module-group-detail')
    status = ChoiceField(choices=MODULE_GROUP_STATUS_CHOICES)

    class Meta:
        model = ModuleGroup
        fields = ['url', 'id', 'code', 'name', 'status', 'slug']


class NestedServiceUserSerializer(WritableNestedSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='service-api:service-user-detail')
    service = NestedServiceSerializer()
    instance = NestedInstanceSerializer()

    class Meta:
        model = ServiceUser
        fields = ['url', 'id', 'service', 'instance', 'username']
