from service import filters
from service.api import serializers
from service.models import (
    ModuleGroup, ServiceGroup, Service, ServiceContact, ServiceUser,
    ServiceManager, ServiceDocument, ServiceLicense, Module,
)
from utilities.api import ModelViewSet


#
# Service Group
#
class ServiceGroupViewSet(ModelViewSet):
    queryset = ServiceGroup.objects.all()
    serializer_class = serializers.ServiceGroupSerializer
    filterset_class = filters.ServiceGroupFilter


#
# Service
#
class ServiceViewSet(ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = serializers.ServiceSerializer
    filterset_class = filters.ServiceFilter


#
# Service Contact
#
class ServiceContactViewSet(ModelViewSet):
    queryset = ServiceContact.objects.all()
    serializer_class = serializers.ServiceContactSerializer
    filterset_class = filters.ServiceContactFilter


#
# Service User
#
class ServiceUserViewSet(ModelViewSet):
    queryset = ServiceUser.objects.all()
    serializer_class = serializers.ServiceUserSerializer
    filterset_class = filters.ServiceUserFilter


#
# Service Manager
#
class ServiceManagerViewSet(ModelViewSet):
    queryset = ServiceManager.objects.all()
    serializer_class = serializers.ServiceManagerSerializer
    filterset_class = filters.ServiceManagerFilter


#
# Service Document
#
class ServiceDocumentViewSet(ModelViewSet):
    queryset = ServiceDocument.objects.all()
    serializer_class = serializers.ServiceDocumentSerializer
    filterset_class = filters.ServiceDocumentFilter


#
# Service License
#
class ServiceLicenseViewSet(ModelViewSet):
    queryset = ServiceLicense.objects.all()
    serializer_class = serializers.ServiceLicenseSerializer
    filterset_class = filters.ServiceLicenseFilter


#
# Module Group
#
class ModuleGroupViewSet(ModelViewSet):
    queryset = ModuleGroup.objects.all()
    serializer_class = serializers.ModuleGroupSerializer
    filterset_class = filters.ModuleGroupFilter


#
# Module
#
class ModuleViewSet(ModelViewSet):
    queryset = Module.objects.all()
    serializer_class = serializers.ModuleSerializer
    filterset_class = filters.ModuleFilter
