from rest_framework import serializers
from taggit_serializer.serializers import TaggitSerializer, TagListSerializerField

from dcim.api.nested_serializers import NestedManagerSerializer, NestedInstanceSerializer
from service.api.nested_serializers import (
    NestedServiceSerializer, NestedServiceGroupSerializer, NestedModuleGroupSerializer,
    NestedServiceUserSerializer,
)
from service.constants import *
from service.models import (
    ServiceGroup, Service, ServiceContact, ServiceUser, ServiceManager,
    ServiceDocument, ServiceLicense, ModuleGroup, Module,
)
from tenancy.api.nested_serializers import NestedTenantSerializer
from utilities.api import (
    ValidatedModelSerializer, ChoiceField
)


class ServiceGroupSerializer(ValidatedModelSerializer):
    tenant = NestedTenantSerializer()
    status = ChoiceField(choices=SERVICE_GROUP_STATUS_CHOICES)

    class Meta:
        model = ServiceGroup
        fields = ['id', 'code', 'name', 'status', 'tenant', 'description', 'slug', 'note']


class ServiceSerializer(TaggitSerializer, ValidatedModelSerializer):
    manager = NestedManagerSerializer()
    service_group = NestedServiceGroupSerializer(required=False, allow_null=True)
    status = ChoiceField(choices=SERVICE_STATUS_CHOICES)
    tenant = NestedTenantSerializer()
    backup_type = ChoiceField(choices=SERVICE_BACKUP_TYPE_CHOICES)
    level_important = ChoiceField(choices=SERVICE_LEVEL_IMPORTANT_CHOICES)
    enduser = ChoiceField(choices=SERVICE_END_USER_CHOICES)
    customer = ChoiceField(choices=SERVICE_CUSTOMER_CHOICES)
    user_influence = ChoiceField(choices=SERVICE_USER_INFLUENCE_CHOICES)
    kpi = ChoiceField(choices=SERVICE_KPI_METHOD_CHOICES)
    verify = ChoiceField(choices=SERVICE_VERIFY_CHOICES)
    monitored = ChoiceField(choices=SERVICE_MONITOR_STATUS_CHOICES)
    dev_unit = ChoiceField(choices=SERVICE_DEV_UNIT_CHOICES)
    dedicated = ChoiceField(choices=SERVICE_DEDICATED_CHOICES)
    tags = TagListSerializerField(required=False)

    class Meta:
        model = Service
        fields = [
            'id', 'code', 'name', 'manager', 'service_group', 'status', 'start_date', 'stop_date', 'tenant',
            'backup_type', 'level_important', 'enduser', 'customer', 'user_influence', 'kpi', 'peak_time',
            'low_time', 'max_exchange', 'avg_exchange', 'purpose', 'description', 'reason_deactive', 'verify',
            'monitored', 'dev_unit', 'dedicated', 'reason_not_monitored', 'monitored_off_time', 'slug', 'tags',
            'created', 'last_updated',
        ]


class ServiceContactSerializer(ValidatedModelSerializer):
    service = NestedServiceSerializer()

    class Meta:
        model = ServiceContact
        fields = ['id', 'service', 'name', 'email', 'phone', 'address', 'description', 'note']


class ServiceUserSerializer(ValidatedModelSerializer):
    service = NestedServiceSerializer()
    instance = NestedInstanceSerializer()

    class Meta:
        model = ServiceUser
        fields = ['id', 'service', 'instance', 'username', 'description', 'note']


class ServiceManagerSerializer(ValidatedModelSerializer):
    service = NestedServiceSerializer()
    manager = NestedManagerSerializer()
    responsible = ChoiceField(choices=SERVICE_MANAGER_RESPONSIBLE_CHOICES)

    class Meta:
        model = ServiceManager
        fields = ['id', 'service', 'manager', 'responsible', 'description', 'note']


class ServiceDocumentSerializer(ValidatedModelSerializer):
    service = NestedServiceSerializer()

    class Meta:
        model = ServiceDocument
        fields = ['id', 'service', 'name', 'attach_file', 'path', 'file_name', 'description', 'note']


class ServiceLicenseSerializer(ValidatedModelSerializer):
    service = NestedServiceSerializer()
    type = ChoiceField(choices=SERVICE_LICENSE_TYPE_CHOICES)
    start_time = serializers.DateTimeField()
    expire_time = serializers.DateTimeField()
    alarm_time = serializers.DateTimeField()

    class Meta:
        model = ServiceLicense
        fields = ['id', 'service', 'license', 'type', 'value', 'start_time', 'expire_time', 'alarm_time', 'description',
                  'note']


class ModuleGroupSerializer(TaggitSerializer, ValidatedModelSerializer):
    tags = TagListSerializerField(required=False)
    status = ChoiceField(choices=MODULE_GROUP_STATUS_CHOICES)

    class Meta:
        model = ModuleGroup
        fields = ['id', 'code', 'name', 'status', 'description', 'slug', 'tags']


class ModuleSerializer(TaggitSerializer, ValidatedModelSerializer):
    service = NestedServiceSerializer()
    group = NestedModuleGroupSerializer(required=False, allow_null=True)
    type = ChoiceField(choices=MODULE_TYPE_CHOICES)
    service_user = NestedServiceUserSerializer()
    pro_language = ChoiceField(choices=MODULE_PROGRAMMING_LANGUAGE_CHOICES)
    web_server = ChoiceField(choices=MODULE_WEB_SERVER_CHOICES)
    status = ChoiceField(choices=MODULE_STATUS_CHOICES)
    real_status = ChoiceField(choices=MODULE_REAL_STATUS_CHOICES)
    stop_date = serializers.DateTimeField(format="%Y-%m-%dT%H:%M:%SZ", required=False)
    tags = TagListSerializerField(required=False)

    class Meta:
        model = Module
        fields = [
            'id', 'service', 'group', 'code', 'name', 'type', 'service_user', 'module_function', 'pro_language',
            'web_server', 'status', 'real_status', 'port_mm', 'uptime', 'purpose', 'description', 'stop_date',
            'reason_deactive', 'slug', 'tags'
        ]
