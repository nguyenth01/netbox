from rest_framework import routers

from . import views


class ServiceRootView(routers.APIRootView):
    """
    Service API root view
    """
    def get_view_name(self):
        return 'Service'


router = routers.DefaultRouter()
router.APIRootView = ServiceRootView

# Service
router.register(r'service', views.ServiceViewSet)

# Service Group
router.register(r'service-group', views.ServiceGroupViewSet, basename='service-group')

# Service Contact
router.register(r'service-contact', views.ServiceContactViewSet, basename='service-contact')

# Service User
router.register(r'service-user', views.ServiceUserViewSet, basename='service-user')

# Service Manager
router.register(r'service-manager', views.ServiceManagerViewSet, basename='service-manager')

# Service Document
router.register(r'service-document', views.ServiceDocumentViewSet, basename='service-document')

# Service License
router.register(r'service-license', views.ServiceLicenseViewSet, basename='service-license')

# Module Group
router.register(r'module-group', views.ModuleGroupViewSet, basename='module-group')

# Module
router.register(r'module', views.ModuleViewSet, basename='module')

app_name = 'service-api'
urlpatterns = router.urls
