import django_tables2 as tables
from django_tables2.utils import Accessor
from tenancy.tables import COL_TENANT
from utilities.tables import BaseTable, BooleanColumn, ColorColumn, ToggleColumn

from .models import ServiceGroup, Service, ServiceUser, ServiceContact, ServiceDocument, ServiceLicense, ServiceManager

SERVICEGROUP_ACTION = """
<a href="{% url 'service:servicegroup_changelog' slug=record.slug %}" class="btn btn-default btn-xs" title="Changelog">
    <i class="fa fa-history"></i>
</a>
{% if perms.service.change_servicegroup %}
    <a href="{% url 'service:servicegroup_edit' slug=record.slug %}?return_url={{ request.path }}" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
{% endif %}
"""

SERVICEUSER_ACTION = """
<a href="{% url 'service:serviceuser_changelog' pk=record.pk %}" class="btn btn-default btn-xs" title="Changelog">
    <i class="fa fa-history"></i>
</a>
{% if perms.service.change_serviceuser %}
    <a href="{% url 'service:serviceuser_edit' pk=record.pk %}?return_url={{ request.path }}" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
{% endif %}
"""

SERVICECONTACT_ACTION = """
<a href="{% url 'service:servicecontact_changelog' pk=record.pk %}" class="btn btn-default btn-xs" title="Changelog">
    <i class="fa fa-history"></i>
</a>
{% if perms.service.change_serviceuser %}
    <a href="{% url 'service:servicecontact_edit' pk=record.pk %}?return_url={{ request.path }}" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
{% endif %}
"""

SERVICEDOCUMENT_ACTION = """
<a href="{% url 'service:servicedocument_changelog' pk=record.pk %}" class="btn btn-default btn-xs" title="Changelog">
    <i class="fa fa-history"></i>
</a>
{% if perms.service.change_serviceuser %}
    <a href="{% url 'service:servicedocument_edit' pk=record.pk %}?return_url={{ request.path }}" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
{% endif %}
"""

SERVICE_LICENSE_TYPE = """
<span class="label label-{{ record.get_status_class }}">{{ record.get_status_display }}</span>
"""

SERVICEGROUP_STATUS= """
<span class="label label-{{ record.get_status_class }}">{{ record.get_status_display }}</span>
"""

SERVICE_LINK = """
<a href="{% url 'service:service' pk=record.pk %}">
    {{ record.name|default:'<span class="label label-info">Unnamed service</span>' }}
</a>
"""

MANAGER = """
{{ record.manager.username|default:"Not set" }}
"""

SERVICE_LEVEL_IMPORTANT_LABEL = """
<span class="label label-{{ record.get_level_important_class }}">{{ record.get_level_important_display }}</span>
"""

STATUS_LABEL = """
<span class="label label-{{ record.get_status_class }}">{{ record.get_status_display }}</span>
"""

COL_SERVICE = """
{% if record.service %}
    <a href="{% url 'service:service' slug=record.service.slug %}" >{{ record.service }}</a>
{% else %}
    &mdash;
{% endif %}
"""

# SERVICEGROUP_SERVICE_COUNT = """
# <a href="{% url 'service:service_list' %}?group={{ record.slug }}">{{ value }}</a>
# """

class ServiceGroupTable(BaseTable):
    pk = ToggleColumn()
    code = tables.Column(verbose_name='code')
    name = tables.Column(verbose_name='name')
    status = tables.TemplateColumn(template_code=SERVICEGROUP_STATUS)
    tenant = tables.TemplateColumn(template_code=COL_TENANT)
    slug = tables.Column()
    actions = tables.TemplateColumn(
        template_code=SERVICEGROUP_ACTION,
        attrs={'td': {'class': 'text-right noprint'}},
        verbose_name = ''
    )

    class Meta(BaseTable.Meta):
        model: ServiceGroup
        fields: ('pk', 'code', 'name', 'status', 'tenant_id', 'slug', 'description', 'note')

    
# 
# Service
#  

class ServiceTable(BaseTable):
    pk = ToggleColumn()
    name = tables.TemplateColumn(
        verbose_name="Name",
        template_code=SERVICE_LINK
    )
    code = tables.Column(verbose_name='code')
    manager = tables.TemplateColumn(MANAGER, verbose_name='Manager')
    status = tables.TemplateColumn(template_code = STATUS_LABEL)
    level_important = tables.TemplateColumn(SERVICE_LEVEL_IMPORTANT_LABEL, verbose_name='Level Important')
    tenant = tables.TemplateColumn(template_code=COL_TENANT)
    # slug = tables.Column(verbose_name='Slug')
    class Meta(BaseTable.Meta):
        model = Service
        fields = (
            'pk', 'code', 'name', 'manager', 'service_group', 'level_important', 'start_date',
            'stop_date', 'tenant',
        )


class ServiceImportTable(BaseTable):
    name = tables.TemplateColumn(
        verbose_name="Name",
        template_code=SERVICE_LINK
    )
    code = tables.Column(verbose_name='code')
    manager = tables.TemplateColumn(MANAGER, verbose_name='Manager')
    group = tables.Column(accessor=Accessor('group.name'), verbose_name='Group')
    status = tables.TemplateColumn(STATUS_LABEL)
    level_important = tables.TemplateColumn(SERVICE_LEVEL_IMPORTANT_LABEL, verbose_name='Level Important')
    start_date = tables.Column(verbose_name='start date')
    stop_date = tables.Column(verbose_name='stop date')
    # tenant = tables.TemplateColumn(template_code=COL_TENANT)
    slug = tables.Column(verbose_name='Slug')
    class Meta(BaseTable.Meta):
        model = Service
        fields = (
            'pk', 'code', 'name', 'manager', 'group', 'level_important', 'start_date',
            'stop_date', 'tenant',  'manager','backup_type', 'enduser', 'customer', 
            'user_influence', 'kpi', 'verify', 'monitored', 'dev_unit', 'dedicated'
        )

# 
# Service User
# 

class ServiceUserTable(BaseTable):
    pk = ToggleColumn()
    service = tables.Column(verbose_name = 'service')
    username = tables.Column(verbose_name='user name')
    instance = tables.Column(verbose_name = 'instance')

    actions = tables.TemplateColumn(
        template_code=SERVICEUSER_ACTION,
        attrs={'td': {'class': 'text-right noprint'}},
        verbose_name = ''
    )

    class Meta(BaseTable.Meta):
        model: ServiceUser
        fields: ('pk', 'instance', 'username', 'service', 'description' )


# 
# Service contact
# 

class ServiceContactTable(BaseTable):
    pk = ToggleColumn()
    service = tables.LinkColumn()
    name = tables.Column(verbose_name='name')
    email = tables.Column(verbose_name='email')
    phone = tables.Column(verbose_name='phone')
    address = tables.Column(verbose_name='address')
    description = tables.Column(verbose_name='description')
    actions = tables.TemplateColumn(
        template_code=SERVICECONTACT_ACTION,
        attrs={'td': {'class': 'text-right noprint'}},
        verbose_name = ''
    )

    class Meta(BaseTable.Meta):
        model: ServiceContact
        fields: ('service', 'name', 'email', 'phone', 'address', 'description', 'note' )


# 
# Service document
# 

class ServiceDocumentTable(BaseTable):
    pk = ToggleColumn()
    service = tables.LinkColumn()
    name = tables.Column(verbose_name='name')
    attach_file = tables.Column(verbose_name='attach file')
    path = tables.Column(verbose_name='path')
    file_name = tables.Column(verbose_name='file name')
    description = tables.Column(verbose_name='description')
    actions = tables.TemplateColumn(
        template_code=SERVICEDOCUMENT_ACTION,
        attrs={'td': {'class': 'text-right noprint'}},
        verbose_name = ''
    )

    class Meta(BaseTable.Meta):
        model: ServiceDocument
        fields: ('service', 'name', 'attach_file', 'path', 'file_name', 'description', 'note' )


# 
# Service license
# 

class ServiceLicenseTable(BaseTable):
    pk = ToggleColumn()
    service = tables.LinkColumn()
    license = tables.Column(verbose_name='license')
    type = tables.TemplateColumn(SERVICE_LICENSE_TYPE, verbose_name='type')
    value = tables.Column(verbose_name='value')
    start_time = tables.Column(verbose_name='start time')
    expire_time = tables.Column(verbose_name='expire time')
    alarm_time = tables.Column(verbose_name='alarm time')
    description = tables.Column(verbose_name='description')
    actions = tables.TemplateColumn(
        template_code=SERVICEDOCUMENT_ACTION,
        attrs={'td': {'class': 'text-right noprint'}},
        verbose_name = ''
    )

    class Meta(BaseTable.Meta):
        model: ServiceLicense
        fields: ('service', 'license', 'type', 'value', 'start_time', 'expire_time', 'alarm_time', 'description', 'note')


