from django import forms
from django.db.models import Count
from taggit.forms import TagField
from django.contrib.auth.models import User

from extras.forms import AddRemoveTagsForm, CustomFieldForm, CustomFieldBulkEditForm, CustomFieldFilterForm
from utilities.forms import (
    APISelect, APISelectMultiple,StaticSelect2, BootstrapMixin, ChainedFieldsMixin, ChainedModelChoiceField, CommentField,
    FilterChoiceField, SlugField, StaticSelect2Multiple, CSVChoiceField, add_blank_choice, CSVDataField
)
from .models import Service, ServiceGroup, ServiceUser, ServiceContact, ServiceDocument, ServiceLicense
from .constants import *
from tenancy.forms import TenancyForm
from tenancy.models import Tenant
from django.utils import formats
from dcim.models import Instance

# 
#  Service Group
# 

class ServiceGroupForm(BootstrapMixin, forms.ModelForm):
    slug = SlugField()

    class Meta:
        model = ServiceGroup
        fields = [
            'code', 'name', 'status', 'tenant', 'slug'
        ]


class ServiceGroupCSVForm(forms.ModelForm):
    slug = SlugField()
    ####################################
    tenant = forms.ModelChoiceField(
        queryset=Tenant.objects.all(),
        to_field_name='name',
    )
    class Meta:
        model = ServiceGroup
        fields = ServiceGroup.csv_headers
        help_texts = {
            'name': 'Name of service group',
        }


# 
# Service
# 

class ServiceFilterForm(BootstrapMixin, CustomFieldFilterForm):
    model = Service
    q = forms.CharField(
        required=False,
        label='Search'
    )
    manager = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        required=False,
        to_field_name='username',
        widget=StaticSelect2Multiple()
    )
    group = FilterChoiceField(
        queryset=ServiceGroup.objects.all(),
        to_field_name='slug',
        widget=APISelectMultiple(
            api_url="/api/service/service-group/",
            value_field="slug",
        )
    )
    
    tenant = FilterChoiceField(
        queryset=Tenant.objects.all(),
        to_field_name='slug',
        null_label='-- None --',
        widget=APISelectMultiple(
            api_url="/api/tenancy/tenants/",
            value_field="slug",
            null_option=True,
        )
    )
    status = forms.MultipleChoiceField(
        choices=SERVICE_STATUS_CHOICES,
        required=False,
        widget=StaticSelect2Multiple()
    )
    level_important = forms.MultipleChoiceField(
        choices=SERVICE_LEVEL_IMPORTANT_CHOICES,
        required=False,
        widget=StaticSelect2Multiple()
    )

class ServiceForm(BootstrapMixin, TenancyForm, CustomFieldForm):
    slug = SlugField()
    tenant = forms.ModelChoiceField(
        queryset=Tenant.objects.all(),
        required=False,
        widget=APISelect(
            api_url='/api/tenancy/tenant/',
        )
    )
        
    class Meta:
        model = Service
        fields = [
             'code', 'name', 'manager', 'service_group', 'status', 'tenant',
             'start_date', 'stop_date', 'enduser', 'customer', 'backup_type'
        ]
        

class ServiceCSVForm(forms.ModelForm):
    # slug = SlugField()
    manager = forms.ModelChoiceField(
        queryset=User.objects.all(),
        to_field_name='username',
        help_text='Manager account name',
        error_messages={
            'invalid_choice': 'Invalid manager account name.',
        }
    )
    service_group = forms.ModelChoiceField(
        queryset=ServiceGroup.objects.all(),
        to_field_name='name',
    )
    tenant = forms.ModelChoiceField(
        queryset=Tenant.objects.all(),
        required=True,
        to_field_name='name',
        help_text='Name of assigned tenant',
        error_messages={
            'invalid_choice': 'Tenant not found.',
        }
    )
    
    status = CSVChoiceField(
        choices=SERVICE_STATUS_CHOICES,
        help_text='Operational status'
    )
    level_important = CSVChoiceField(
        choices=SERVICE_LEVEL_IMPORTANT_CHOICES,
        help_text='Operational status'
    )

    
    class Meta:
        model = Service
        fields = Service.csv_headers

class ServiceBulkEditForm(BootstrapMixin, AddRemoveTagsForm, CustomFieldBulkEditForm):
    pk = forms.ModelMultipleChoiceField(
        queryset=Service.objects.all(),
        widget=forms.MultipleHiddenInput()
    )

    service_group = forms.ModelChoiceField(
        queryset=ServiceGroup.objects.all(),
        required=False,
        label='group',
        widget=APISelect(
            api_url="/api/service/service-group/"
        )
    )
    tenant = forms.ModelChoiceField(
        queryset=Tenant.objects.all(),
        required=False,
        widget=APISelect(
            api_url="/api/tenancy/tenants/"
        )
    )
    
    status = forms.ChoiceField(
        choices=add_blank_choice(SERVICE_STATUS_CHOICES),
        required=False,
        initial='',
        widget=StaticSelect2()
    )
    level_important = forms.ChoiceField(
        choices=add_blank_choice(SERVICE_LEVEL_IMPORTANT_CHOICES),
        required=False,
        initial='',
        widget=StaticSelect2()
    )
    monitored_off_time = forms.DateField(
        required = False
    )
    start_date = forms.DateField(
        required = False
    )
    stop_date = forms.DateField(
        required = False
    )
    class Meta:
        nullable_fields = [
            
        ]

# 
# service user
# 

class ServiceUserForm(BootstrapMixin, forms.ModelForm):
    service = forms.ModelChoiceField(
        queryset=Service.objects.all(),
        to_field_name='name',
    )
    instance = forms.ModelChoiceField(
        queryset=Instance.objects.all(),
        to_field_name='name',
    )
    class Meta:
        model = ServiceUser
        fields = [
            'instance', 'username', 'service', 'description'
        ]


class ServiceUserCSVForm(forms.ModelForm):
    
    service = forms.ModelChoiceField(
        queryset=Service.objects.all(),
        to_field_name='name',
    )
    class Meta:
        model = ServiceUser
        fields = [
            'instance', 'username', 'service', 'description', 'note'
        ]

# 
# service contact
# 

class ServiceContactForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = ServiceContact
        fields = [
            'service', 'name', 'email', 'phone', 'address', 'description', 'note'
        ]


class ServiceContactCSVForm(forms.ModelForm):
    
    service = forms.ModelChoiceField(
        queryset=Service.objects.all(),
        to_field_name='name',
    )
    class Meta:
        model = ServiceContact
        fields = [
            'service', 'name', 'email', 'phone', 'address', 'description', 'note'
        ]

# 
# service document
# 

class ServiceDocumentForm(BootstrapMixin, forms.ModelForm):

    class Meta:
        model = ServiceDocument
        fields = [
            'service', 'name', 'attach_file', 'path', 'file_name', 'description', 'note'
        ]


class ServiceDocumentCSVForm(forms.ModelForm):
    
    service = forms.ModelChoiceField(
        queryset=Service.objects.all(),
        to_field_name='name',
    )
    class Meta:
        model = ServiceDocument
        fields = [
            'service', 'name', 'attach_file', 'path', 'file_name', 'description', 'note'
        ]

# 
# service license
# 

class ServiceLicenseForm(BootstrapMixin, forms.ModelForm):
    
    service = forms.ModelChoiceField(
        queryset=Service.objects.all(),
        to_field_name='name',
    )
    class Meta:
        model = ServiceLicense
        fields = [
            'service', 'license', 'type', 'value', 'start_time', 'expire_time', 'alarm_time', 'description', 'note'
        ]


class ServiceLicenseCSVForm(forms.ModelForm):
    
    type = CSVChoiceField(
        choices=SERVICE_LICENSE_TYPE_CHOICES,
    )

    service = forms.ModelChoiceField(
        queryset=Service.objects.all(),
        to_field_name='name',
    )
    class Meta:
        model = ServiceLicense
        fields = [
            'service', 'license', 'type', 'value', 'start_time', 'expire_time', 'alarm_time', 'description', 'note'
        ]

