import django_filters
from django.contrib.auth.models import User
from dcim.models import Instance
from service.constants import *
from service.models import (
    ServiceUser, ServiceManager, ServiceDocument, ServiceLicense, ServiceGroup,
    Service, ServiceContact, ModuleGroup, Module,

)
from tenancy.models import Tenant
from utilities.filters import NumericInFilter, TagFilter


class ServiceGroupFilter(django_filters.FilterSet):

    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    tenant_id = django_filters.ModelMultipleChoiceFilter(
        field_name='tenant_id',
        queryset=Tenant.objects.all(),
        label='Tenant (ID)',
    )
    tenant = django_filters.ModelMultipleChoiceFilter(
        field_name='tenant__slug',
        queryset=Tenant.objects.all(),
        to_field_name='slug',
        label='Tenant (slug)',
    )

    class Meta:
        model = ServiceGroup
        fields = [
            'code', 'name',
        ]


class ServiceFilter(django_filters.FilterSet):

    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    tenant_id = django_filters.ModelMultipleChoiceFilter(
        field_name='tenant_id',
        queryset=Tenant.objects.all(),
        label='Tenant (ID)',
    )
    tenant = django_filters.ModelMultipleChoiceFilter(
        field_name='tenant__slug',
        queryset=Tenant.objects.all(),
        to_field_name='slug',
        label='Tenant (slug)',
    )

    class Meta:
        model = Service
        fields = [
            'code', 'name',
        ]


class ServiceContactFilter(django_filters.FilterSet):

    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    service_id = django_filters.ModelMultipleChoiceFilter(
        field_name='service_id',
        queryset=Service.objects.all(),
        label='Service (ID)',
    )
    service = django_filters.ModelMultipleChoiceFilter(
        field_name='service__code',
        queryset=Service.objects.all(),
        to_field_name='code',
        label='Service (code)',
    )

    class Meta:
        model = ServiceContact
        fields = [
            'name', 'email',
        ]


class ServiceUserFilter(django_filters.FilterSet):

    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    service_id = django_filters.ModelMultipleChoiceFilter(
        field_name='service_id',
        queryset=Service.objects.all(),
        label='Service (ID)',
    )
    service = django_filters.ModelMultipleChoiceFilter(
        field_name='service__code',
        queryset=Service.objects.all(),
        to_field_name='code',
        label='Service (code)',
    )
    instance_id = django_filters.ModelMultipleChoiceFilter(
        field_name='instance_id',
        queryset=Instance.objects.all(),
        label='Instance (ID)',
    )
    instance = django_filters.ModelMultipleChoiceFilter(
        field_name='instance__name',
        queryset=Instance.objects.all(),
        to_field_name='name',
        label='Instance (name)',
    )

    class Meta:
        model = ServiceUser
        fields = [
            'username',
        ]


class ServiceManagerFilter(django_filters.FilterSet):

    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    service_id = django_filters.ModelMultipleChoiceFilter(
        field_name='service_id',
        queryset=Service.objects.all(),
        label='Service (ID)',
    )
    service = django_filters.ModelMultipleChoiceFilter(
        field_name='service__code',
        queryset=Service.objects.all(),
        to_field_name='code',
        label='Service (code)',
    )
    manager_id = django_filters.ModelMultipleChoiceFilter(
        field_name='manager_id',
        queryset=User.objects.all(),
        label='Manager(ID)',
    )
    manager = django_filters.ModelMultipleChoiceFilter(
        field_name='manager__username',
        to_field_name='username',
        queryset=User.objects.all(),
        label='Manager (username)',
    )

    class Meta:
        model = ServiceManager
        fields = [
            'responsible',
        ]


class ServiceDocumentFilter(django_filters.FilterSet):

    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    service_id = django_filters.ModelMultipleChoiceFilter(
        field_name='service_id',
        queryset=Service.objects.all(),
        label='Service (ID)',
    )
    service = django_filters.ModelMultipleChoiceFilter(
        field_name='service__code',
        queryset=Service.objects.all(),
        to_field_name='code',
        label='Service (code)',
    )

    class Meta:
        model = ServiceDocument
        fields = [
            'name',
        ]


class ServiceLicenseFilter(django_filters.FilterSet):

    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    service_id = django_filters.ModelMultipleChoiceFilter(
        field_name='service_id',
        queryset=Service.objects.all(),
        label='Service (ID)',
    )
    service = django_filters.ModelMultipleChoiceFilter(
        field_name='service__code',
        queryset=Service.objects.all(),
        to_field_name='code',
        label='Service (code)',
    )

    class Meta:
        model = ServiceLicense
        fields = [
            'value',
        ]


class ModuleGroupFilter(django_filters.FilterSet):
    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in',
    )
    status = django_filters.MultipleChoiceFilter(
        choices=MODULE_GROUP_STATUS_CHOICES,
        null_value=None
    )

    class Meta:
        model = ModuleGroup
        fields = [
            'code', 'name',
        ]


class ModuleFilter(django_filters.FilterSet):
    id__in = NumericInFilter(
        field_name='id',
        lookup_expr='in'
    )
    service_id = django_filters.ModelMultipleChoiceFilter(
        field_name='service_id',
        queryset=Service.objects.all(),
        label='Service (ID)',
    )
    service = django_filters.ModelMultipleChoiceFilter(
        field_name='service__code',
        queryset=Service.objects.all(),
        to_field_name='code',
        label='Service (code)',
    )
    group_id = django_filters.ModelMultipleChoiceFilter(
        field_name='group_id',
        queryset=ModuleGroup.objects.all(),
        label='Module Group (ID)',
    )
    group = django_filters.ModelMultipleChoiceFilter(
        field_name='group__code',
        queryset=ModuleGroup.objects.all(),
        to_field_name='code',
        label='Module Group (code)',
    )
    status = django_filters.MultipleChoiceFilter(
        choices=MODULE_GROUP_STATUS_CHOICES,
        null_value=None
    )
    tag = TagFilter()

    class Meta:
        model = Module
        fields = [
            'code', 'name',
        ]
