from django.contrib.auth.models import User
from django.urls import reverse
from django.utils.dateparse import parse_datetime
from rest_framework import status
from rest_framework.utils import json

from dcim.models import Site, Manufacturer, DeviceType, DeviceRole, Device, InstanceRole, Instance
from service.constants import *
from service.models import (
    ServiceUser, ServiceManager, ServiceDocument, ServiceLicense, ServiceGroup,
    Service, ServiceContact, ModuleGroup, Module,
)
from tenancy.models import Tenant
from utilities.testing import APITestCase


class ServiceAPITest(APITestCase):
    def setUp(self):
        super().setUp()
        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.user1 = User.objects.create(username='testuser1')
        self.user2 = User.objects.create(username='testuser2')
        self.service1 = Service.objects.create(
            code='VCR_IT_IPCC_069',
            name='WEB THI ONLINE',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_ACTIVE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
            level_important=SERVICE_LEVEL_NORMAL,
            enduser=SERVICE_END_USER_VTNET,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VCR_IT_IPCC_069'
        )
        self.service2 = Service.objects.create(
            code='NAT_IT_BILL_004',
            name='Payment Process',
            manager=self.user2,
            service_group=self.service_group1,
            status=SERVICE_STATUS_DISABLE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='NAT_IT_BILL_004',
        )

        self.site = Site.objects.create(name='Site 1', slug='site-1')
        self.manufacturer = Manufacturer.objects.create(name='Manufacturer', slug='manufacturer-1')
        self.device_role = DeviceRole.objects.create(name='Device Role', slug='device-role-1')
        self.device_type = DeviceType.objects.create(
            manufacturer=self.manufacturer, model='Device Type 1', slug='device-type-1'
        )
        self.device1 = Device.objects.create(
            device_type=self.device_type,
            device_role=self.device_role,
            name='Device 1',
            site=self.site,
            manager=self.user1
        )
        self.instancerole1 = InstanceRole.objects.create(
            name='Test Instance Role 1', slug='test-instance-role-1', color='ff0000'
        )
        self.instancerole2 = InstanceRole.objects.create(
            name='Test Instance Role 2', slug='test-instance-role-2', color='00ff00'
        )
        self.instance1 = Instance.objects.create(name='Instance 1', device=self.device1,
                                                 instance_role=self.instancerole1, manager=self.user1)
        self.instance2 = Instance.objects.create(name='Instance 2', device=self.device1,
                                                 instance_role=self.instancerole1, manager=self.user2)
        self.instance3 = Instance.objects.create(name='Instance 3', device=self.device1,
                                                 instance_role=self.instancerole1, manager=self.user1)

        self.service_user1 = ServiceUser.objects.create(
            service=self.service1, instance=self.instance1, username='app1', description='sv_user1', note='note_user1'
        )
        self.service_user2 = ServiceUser.objects.create(
            service=self.service1, instance=self.instance2, username='app1', description='sv_user2', note='note_user2'
        )
        self.service_user3 = ServiceUser.objects.create(
            service=self.service2, instance=self.instance3, username='app2', description='sv_user3', note='note_user3'
        )
        self.service_user4 = ServiceUser.objects.create(
            service=self.service1, instance=self.instance3, username='app1', description='sv_user4', note='note_user4'
        )
        self.service_user5 = ServiceUser.objects.create(
            service=self.service1, instance=self.instance3, username='app5', description='sv_user5', note='note_user5'
        )

        self.module_group1 = ModuleGroup.objects.create(
            code='test-module-group-1',
            name='Test Module Group 1',
            status=MODULE_GROUP_STATUS_ACTIVE,
            description='Test Module Group 1 Description',
            slug='test-module-group-1'
        )
        self.module_group2 = ModuleGroup.objects.create(
            code='test-module-group-2',
            name='Test Module Group 2',
            status=MODULE_GROUP_STATUS_ACTIVE,
            description='Test Module Group 2 Description',
            slug='test-module-group-2'
        )
        self.module_group3 = ModuleGroup.objects.create(
            code='test-module-group-3',
            name='Test Module Group 3',
            status=MODULE_GROUP_STATUS_ACTIVE,
            description='Test Module Group 3 Description',
            slug='test-module-group-3'
        )
        self.module_group4 = ModuleGroup.objects.create(
            code='test-module-group-4',
            name='Test Module Group 4',
            status=MODULE_GROUP_STATUS_DEACTIVE,
            description='Test Module Group 4 Description',
            slug='test-module-group-4'
        )

        self.module1 = Module.objects.create(
            service=self.service1,
            group=self.module_group1,
            code='test_module_1',
            name='Test Module 1',
            type=MODULE_TYPE_APACHE_KAFKA,
            service_user=self.service_user1,
            module_function='Test Module 1 Function',
            pro_language=MODULE_PROGRAMMING_LANGUAGE_JAVA,
            web_server=MODULE_WEB_SERVER_Nginx,
            status=MODULE_STATUS_RUNNING,
            real_status=MODULE_REAL_STATUS_ON,
            port_mm=8086,
            uptime='3-06:06:36',
            purpose='Test Module 1 Purpose',
            description='Test Module 1 Description',
            slug='test-module-1'
        )
        self.module2 = Module.objects.create(
            service=self.service1,
            group=self.module_group1,
            code='test_module_2',
            name='Test Module 2',
            type=MODULE_TYPE_APPCHE_WEB,
            service_user=self.service_user2,
            module_function='Test Module 2 Function',
            pro_language=MODULE_PROGRAMMING_LANGUAGE_JAVA,
            web_server=MODULE_WEB_SERVER_Nginx,
            status=MODULE_STATUS_STOPPED,
            real_status=MODULE_REAL_STATUS_OFF,
            uptime='18-06:26:13',
            purpose='Test Module 2 Purpose',
            description='Test Module 2 Description',
            stop_date=parse_datetime('2016-10-03T19:00:00Z'),
            reason_deactive='Off for maintenance',
            slug='test-module-2'
        )
        self.module3 = Module.objects.create(
            service=self.service2,
            code='test_module_3',
            name='Test Module 3',
            type=MODULE_TYPE_GLASSFISH_WEB,
            service_user=self.service_user3,
            module_function='Test Module 3 Function',
            pro_language=MODULE_PROGRAMMING_LANGUAGE_ASP_NET,
            web_server=MODULE_WEB_SERVER_Nginx,
            status=MODULE_STATUS_RUNNING,
            real_status=MODULE_REAL_STATUS_ON,
            uptime='18-06:40:58',
            purpose='Test Module 3 Purpose',
            description='Test Module 3 Description',
            slug='test-module-3'
        )


class ServiceGroupTest(APITestCase):

    def setUp(self):
        super().setUp()
        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.tenant2 = Tenant.objects.create(name='Test Tenant 2', slug='test-tenant-2')
        self.tenant3 = Tenant.objects.create(name='Test Tenant 3', slug='test-tenant-3')

        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.service_group2 = ServiceGroup.objects.create(
            code='MYT_IT_VAS', name='VAS Application', tenant=self.tenant1, status=SERVICE_GROUP_DEACTIVE,
            slug='my-it-vas'
        )
        self.service_group3 = ServiceGroup.objects.create(
            code='MYT_IT_BILLING', name='BILLING APPLICATION', tenant=self.tenant2, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon-system'
        )

    def test_list_service_group(self):
        url = reverse('service-api:service-group-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_get_service_group_detail(self):
        url = reverse('service-api:service-group-detail', kwargs={'pk': self.service_group1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), {
            'id': self.service_group1.pk,
            'code': 'DATAMON',
            'name': 'DATAMON',
            'status': {
                'value': 1,
                'label': 'Active'
            },
            'tenant': {
                'id': self.tenant1.pk,
                'url': 'http://testserver/api/tenancy/tenants/' + str(self.tenant1.pk) + '/',
                'name': 'Test Tenant 1',
                'slug': 'test-tenant-1'
            },
            'description': 'DATAMON SYSTEM',
            'slug': 'datamon',
            'note': ''
        })

    def test_create_service_group(self):
        data = {
            'code': 'test-service-group-x',
            'name': 'Test service Group X',
            'status': SERVICE_GROUP_ACTIVE,
            'tenant': self.tenant2.pk,
            'description': 'Test Service Group',
            'slug': 'test-service-group-x',
            'note': ''
        }

        url = reverse('service-api:service-group-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(ServiceGroup.objects.count(), 4)

        service_group_created = ServiceGroup.objects.get(pk=response.data['id'])
        self.assertEqual(service_group_created.code, data['code'])
        self.assertEqual(service_group_created.name, data['name'])
        self.assertEqual(service_group_created.tenant.pk, data['tenant'])
        self.assertEqual(service_group_created.status, data['status'])
        self.assertEqual(service_group_created.slug, data['slug'])

    def test_update_service_group(self):
        data = {
            'code': 'test-service-group-x',
            'name': 'Test service Group X',
            'status': SERVICE_GROUP_DEACTIVE,
            'tenant': self.tenant3.pk,
            'description': 'Test Service Group X',
            'slug': 'test-service-group-x',
        }

        url = reverse('service-api:service-group-detail', kwargs={'pk': self.service_group1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(ServiceGroup.objects.count(), 3)

        service_group1_updated = ServiceGroup.objects.get(pk=self.service_group1.pk)
        self.assertEqual(service_group1_updated.code, data['code'])
        self.assertEqual(service_group1_updated.name, data['name'])
        self.assertEqual(service_group1_updated.tenant.pk, data['tenant'])
        self.assertEqual(service_group1_updated.status, data['status'])
        self.assertEqual(service_group1_updated.slug, data['slug'])

    def test_delete_service_group(self):
        url = reverse('service-api:service-group-detail', kwargs={'pk': self.service_group1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ServiceGroup.objects.count(), 2)


class ServiceTest(APITestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()

        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.user1 = User.objects.create(username='testuser1')
        self.user2 = User.objects.create(username='testuser2')
        self.service1 = Service.objects.create(
            code='VCR_IT_IPCC_069',
            name='WEB THI ONLINE',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_ACTIVE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
            level_important=SERVICE_LEVEL_NORMAL,
            enduser=SERVICE_END_USER_VTNET,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VCR_IT_IPCC_069'
        )
        self.service2 = Service.objects.create(
            code='NAT_IT_BILL_004',
            name='Payment Process',
            manager=self.user2,
            service_group=self.service_group1,
            status=SERVICE_STATUS_DISABLE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='NAT_IT_BILL_004',
        )
        self.service3 = Service.objects.create(
            code='VTN_CNTT_BSS_100',
            name='IPCC 63 chi nhánh',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_CHUA_BAN_GIAO,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VTN_CNTT_BSS_100',
        )

    def test_list_service(self):
        url = reverse('service-api:service-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_get_service_detail(self):
        url = reverse('service-api:service-detail', kwargs={'pk': self.service1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.service1.pk,
            'code': 'VCR_IT_IPCC_069',
            'name': 'WEB THI ONLINE',
            'manager': {
                'id': self.service1.manager.pk,
                'username': 'testuser1',
                'first_name': '',
                'last_name': '',
                'email': ''
            },
            'service_group': {
                'url': 'http://testserver/api/service/service-group/' + str(self.service1.service_group.pk) + '/',
                'id': self.service1.service_group.pk,
                'code': 'DATAMON',
                'name': 'DATAMON',
                'status': self.service1.service_group.status,
                'slug': 'datamon'
            },
            'status': {
                'value': SERVICE_STATUS_ACTIVE,
                'label': 'Active'
            },
            'start_date': None,
            'stop_date': None,
            'tenant': {
                'id': self.tenant1.pk,
                'url': 'http://testserver/api/tenancy/tenants/' + str(self.tenant1.pk) + '/',
                'name': 'Test Tenant 1',
                'slug': 'test-tenant-1'
            },
            'backup_type': {
                'value': SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
                'label': 'Active-Active'
            },
            'level_important': {
                'value': SERVICE_LEVEL_NORMAL,
                'label': 'Normal'
            },
            'enduser': {
                'value': SERVICE_END_USER_VTNET,
                'label': 'VTNET'
            },
            'customer': {
                'value': SERVICE_CUSTOMER_ENTERPRISE,
                'label': 'Enterprise'
            },
            'user_influence': {
                'value': SERVICE_USER_INFLUENCE_DIRECT,
                'label': 'Direct'
            },
            'kpi': {
                'value': SERVICE_KPI_METHOD_ADD_ON,
                'label': 'Add on'
            },
            'peak_time': '',
            'low_time': '',
            'max_exchange': '',
            'avg_exchange': '',
            'purpose': '',
            'description': '',
            'reason_deactive': '',
            'verify': {
                'value': SERVICE_VERIFY_VERIFY,
                'label': 'Verify'
            },
            'monitored': {
                'value': SERVICE_MONITOR__STATUS_MONITORED,
                'label': 'Monitored'
            },
            'dev_unit': {
                'value': SERVICE_DEV_UNIT_1,
                'label': 'Dev Unit 1'
            },
            'dedicated': {
                'value': SERVICE_DEDICATED_DEDICATED,
                'label': 'Dedicated'
            },
            'reason_not_monitored': '',
            'monitored_off_time': None,
            'slug': 'VCR_IT_IPCC_069',
            'tags': [],
            'created': response_json['created'],
            'last_updated': response_json['last_updated']
        })

    def test_create_service(self):
        data = {
            'code': 'Test_Service_X',
            'name': 'Test Service X',
            'manager': self.user2.pk,
            'status': SERVICE_STATUS_CHUA_BAN_GIAO,
            'tenant': self.tenant1.pk,
            'backup_type': SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            'level_important': SERVICE_LEVEL_NORMAL,
            'enduser': SERVICE_END_USER_CUSTOMER,
            'customer': SERVICE_CUSTOMER_TELECOMMUNICATION,
            'user_influence': SERVICE_USER_INFLUENCE_DIRECT,
            'kpi': SERVICE_KPI_METHOD_ADD_ON,
            'verify': SERVICE_VERIFY_NOT_VERIFY,
            'monitored': SERVICE_MONITOR_STATUS_NOT_MONITOR,
            'dev_unit': SERVICE_DEV_UNIT_2,
            'dedicated': SERVICE_DEDICATED_NOT_DEDICATE,
            'slug': 'test-service-x',
            'tags': ['tags1', 'tags2'],
        }

        url = reverse('service-api:service-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(Service.objects.count(), 4)

        service_created = Service.objects.get(pk=response.data['id'])
        self.assertEqual(service_created.code, data['code'])
        self.assertEqual(service_created.name, data['name'])
        self.assertEqual(service_created.manager.pk, data['manager'])
        self.assertEqual(service_created.status, data['status'])
        self.assertEqual(service_created.tenant.pk, data['tenant'])
        self.assertEqual(service_created.slug, data['slug'])

    def test_update_service(self):
        data = {
            'code': 'Test_Service_X',
            'name': 'Test Service X',
            'manager': self.user2.pk,
            'status': SERVICE_STATUS_CHUA_BAN_GIAO,
            'tenant': self.tenant1.pk,
            'backup_type': SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            'level_important': SERVICE_LEVEL_NORMAL,
            'enduser': SERVICE_END_USER_CUSTOMER,
            'customer': SERVICE_CUSTOMER_TELECOMMUNICATION,
            'user_influence': SERVICE_USER_INFLUENCE_DIRECT,
            'kpi': SERVICE_KPI_METHOD_ADD_ON,
            'verify': SERVICE_VERIFY_NOT_VERIFY,
            'monitored': SERVICE_MONITOR_STATUS_NOT_MONITOR,
            'dev_unit': SERVICE_DEV_UNIT_2,
            'dedicated': SERVICE_DEDICATED_NOT_DEDICATE,
            'slug': 'test-service-x',
        }

        url = reverse('service-api:service-detail', kwargs={'pk': self.service1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(Service.objects.count(), 3)

        service1_updated = Service.objects.get(pk=self.service1.pk)
        self.assertEqual(service1_updated.code, data['code'])
        self.assertEqual(service1_updated.name, data['name'])
        self.assertEqual(service1_updated.manager.pk, data['manager'])
        self.assertEqual(service1_updated.status, data['status'])
        self.assertEqual(service1_updated.tenant.pk, data['tenant'])
        self.assertEqual(service1_updated.slug, data['slug'])

    def test_delete_service(self):
        url = reverse('service-api:service-detail', kwargs={'pk': self.service1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Service.objects.count(), 2)


class ServiceContactTest(APITestCase):

    def setUp(self):
        super().setUp()

        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.user1 = User.objects.create(username='testuser1')
        self.user2 = User.objects.create(username='testuser2')
        self.service1 = Service.objects.create(
            code='VCR_IT_IPCC_069',
            name='WEB THI ONLINE',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_ACTIVE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
            level_important=SERVICE_LEVEL_NORMAL,
            enduser=SERVICE_END_USER_VTNET,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VCR_IT_IPCC_069'
        )
        self.service2 = Service.objects.create(
            code='NAT_IT_BILL_004',
            name='Payment Process',
            manager=self.user2,
            service_group=self.service_group1,
            status=SERVICE_STATUS_DISABLE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='NAT_IT_BILL_004',
        )

        self.service_contact1 = ServiceContact.objects.create(
            service=self.service1,
            name='Test Service Contact 1',
            email='servicecontact1@email.com',
            phone='0123456787',
            address='BO_VAS',
            description='Operation Unit',
            note='Swap from USER_CONTACT at Mon Apr 17 16:51:52 ICT 2017',
        )
        self.service_contact2 = ServiceContact.objects.create(
            service=self.service1,
            name='Test Service Contact 2',
            email='servicecontact2@email.com',
            phone='0123456788',
            address='cntt vtt',
            description='Development Unit',
            note='Swap from USER_CONTACT at Mon Apr 17 16:51:52 ICT 2017',
        )
        self.service_contact3 = ServiceContact.objects.create(
            service=self.service2,
            name='Test Service Contact 3',
            email='servicecontact3@email.com',
            phone='0123456789',
            address='vtsoft1',
            description='Business Unit',
            note='Swap from USER_CONTACT at Mon Apr 17 16:51:52 ICT 2017',
        )

    def test_list_service_contact(self):
        url = reverse('service-api:service-contact-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_list_service_contact_filter_by_service_id(self):
        url = reverse('service-api:service-contact-list')
        response = self.client.get(url, {'service_id': self.service1.pk}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

    def test_list_service_contact_filter_by_service_code(self):
        url = reverse('service-api:service-contact-list')
        response = self.client.get(url, {'service': self.service2.code}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)

    def test_get_service_contact_detail(self):
        url = reverse('service-api:service-contact-detail', kwargs={'pk': self.service_contact1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)

        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.service_contact1.pk,
            'service': {
                'url': 'http://testserver/api/service/service/' + str(self.service_contact1.service.pk) + '/',
                'id': self.service_contact1.service.pk,
                'code': 'VCR_IT_IPCC_069',
                'name': 'WEB THI ONLINE',
                'status': {
                    'value': 1,
                    'label': 'Active'
                },
                'slug': 'VCR_IT_IPCC_069'
            },
            'name': 'Test Service Contact 1',
            'email': 'servicecontact1@email.com',
            'phone': '0123456787',
            'address': 'BO_VAS',
            'description': 'Operation Unit',
            'note': 'Swap from USER_CONTACT at Mon Apr 17 16:51:52 ICT 2017'
        })

    def test_create_service_contact(self):
        data = {
            'service': self.service2.pk,
            'name': 'Test Service Contact X',
            'email': 'servicecontactx@email.com',
            'phone': '03456978',
            'address': 'vtsoft2',
            'description': 'Operation Unit',
            'note': 'Swap from USER_CONTACT at Mon Apr 17 16:51:52 ICT 2017',
        }

        url = reverse('service-api:service-contact-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(ServiceContact.objects.count(), 4)

        service_contact_created = ServiceContact.objects.get(pk=response.data['id'])
        self.assertEqual(service_contact_created.service.pk, data['service'])
        self.assertEqual(service_contact_created.name, data['name'])
        self.assertEqual(service_contact_created.email, data['email'])
        self.assertEqual(service_contact_created.phone, data['phone'])
        self.assertEqual(service_contact_created.address, data['address'])
        self.assertEqual(service_contact_created.description, data['description'])
        self.assertEqual(service_contact_created.note, data['note'])

    def test_update_service_contact(self):
        data = {
            'service': self.service2.pk,
            'name': 'Test Service Contact X',
            'email': 'servicecontactx@email.com',
            'phone': '03456978',
            'address': 'vtsoft2',
            'description': 'Operation Unit',
            'note': 'Swap from USER_CONTACT at Mon Apr 17 16:51:52 ICT 2017',
        }

        url = reverse('service-api:service-contact-detail', kwargs={'pk': self.service_contact1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(ServiceContact.objects.count(), 3)

        service_contact1_updated = ServiceContact.objects.get(pk=self.service_contact1.pk)
        self.assertEqual(service_contact1_updated.service.pk, data['service'])
        self.assertEqual(service_contact1_updated.name, data['name'])
        self.assertEqual(service_contact1_updated.email, data['email'])
        self.assertEqual(service_contact1_updated.phone, data['phone'])
        self.assertEqual(service_contact1_updated.address, data['address'])
        self.assertEqual(service_contact1_updated.description, data['description'])
        self.assertEqual(service_contact1_updated.note, data['note'])

    def test_delete_service_contact(self):
        url = reverse('service-api:service-contact-detail', kwargs={'pk': self.service_contact1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ServiceContact.objects.count(), 2)


class ServiceUserTest(APITestCase):

    def setUp(self):
        super().setUp()

        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.user1 = User.objects.create(username='testuser1')
        self.user2 = User.objects.create(username='testuser2')
        self.service1 = Service.objects.create(
            code='VCR_IT_IPCC_069',
            name='WEB THI ONLINE',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_ACTIVE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
            level_important=SERVICE_LEVEL_NORMAL,
            enduser=SERVICE_END_USER_VTNET,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VCR_IT_IPCC_069'
        )
        self.service2 = Service.objects.create(
            code='NAT_IT_BILL_004',
            name='Payment Process',
            manager=self.user2,
            service_group=self.service_group1,
            status=SERVICE_STATUS_DISABLE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='NAT_IT_BILL_004',
        )

        self.site = Site.objects.create(name='Site 1', slug='site-1')
        self.manufacturer = Manufacturer.objects.create(name='Manufacturer', slug='manufacturer-1')
        self.device_role = DeviceRole.objects.create(name='Device Role', slug='device-role-1')
        self.device_type = DeviceType.objects.create(
            manufacturer=self.manufacturer, model='Device Type 1', slug='device-type-1'
        )
        self.device1 = Device.objects.create(
            device_type=self.device_type,
            device_role=self.device_role,
            name='Device 1',
            site=self.site,
            manager=self.user1
        )
        self.instancerole1 = InstanceRole.objects.create(
            name='Test Instance Role 1', slug='test-instance-role-1', color='ff0000'
        )
        self.instancerole2 = InstanceRole.objects.create(
            name='Test Instance Role 2', slug='test-instance-role-2', color='00ff00'
        )
        self.instance1 = Instance.objects.create(name='Instance 1', device=self.device1,
                                                 instance_role=self.instancerole1, manager=self.user1)
        self.instance2 = Instance.objects.create(name='Instance 2', device=self.device1,
                                                 instance_role=self.instancerole1, manager=self.user2)
        self.instance3 = Instance.objects.create(name='Instance 3', device=self.device1,
                                                 instance_role=self.instancerole1, manager=self.user1)

        self.service_user1 = ServiceUser.objects.create(
            service=self.service1, instance=self.instance1, username='app1', description='sv_user1', note='note_user1'
        )
        self.service_user2 = ServiceUser.objects.create(
            service=self.service1, instance=self.instance2, username='app1', description='sv_user2', note='note_user2'
        )
        self.service_user3 = ServiceUser.objects.create(
            service=self.service2, instance=self.instance3, username='app2', description='sv_user3', note='note_user3'
        )

    def test_list_service_user(self):
        url = reverse('service-api:service-user-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_list_service_user_filter_by_service_id(self):
        url = reverse('service-api:service-user-list')
        response = self.client.get(url, {'service_id': self.service1.pk}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

    def test_list_service_user_filter_by_instance_name(self):
        url = reverse('service-api:service-user-list')
        response = self.client.get(url, {'instance': self.instance3.name}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)

    def test_get_service_user_detail(self):
        url = reverse('service-api:service-user-detail', kwargs={'pk': self.service_user1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.service_user1.pk,
            'service': {
                'url': 'http://testserver/api/service/service/' + str(self.service1.pk) + '/',
                'id': self.service1.pk,
                'code': 'VCR_IT_IPCC_069',
                'name': 'WEB THI ONLINE',
                'status': {
                    'value': 1,
                    'label': 'Active'
                },
                'slug': 'VCR_IT_IPCC_069'
            },
            'instance': {
                'id': self.instance1.pk,
                'url': 'http://testserver/api/dcim/instances/' + str(self.instance1.pk) + '/',
                'name': 'Instance 1'
            },
            'username': 'app1',
            'description': 'sv_user1',
            'note': 'note_user1'
        })

    def test_create_service_user(self):
        data = {
            'service': self.service2.pk,
            'instance': self.instance1.pk,
            'username': 'app2',
            'description': 'Deployment 2 for service 2 in Instance 1',
            'note': 'Note service user x',
        }

        url = reverse('service-api:service-user-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(ServiceUser.objects.count(), 4)

        service_user_created = ServiceUser.objects.get(pk=response.data['id'])
        self.assertEqual(service_user_created.service.pk, data['service'])
        self.assertEqual(service_user_created.instance.pk, data['instance'])
        self.assertEqual(service_user_created.username, data['username'])
        self.assertEqual(service_user_created.description, data['description'])
        self.assertEqual(service_user_created.note, data['note'])

    def test_update_service_user(self):
        data = {
            'service': self.service2.pk,
            'instance': self.instance1.pk,
            'username': 'app2',
            'description': 'Deployment 2 for service 2 in Instance 1',
            'note': 'Note service user x',
        }

        url = reverse('service-api:service-user-detail', kwargs={'pk': self.service_user1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(ServiceUser.objects.count(), 3)

        service_user1_updated = ServiceUser.objects.get(pk=self.service_user1.pk)
        self.assertEqual(service_user1_updated.service.pk, data['service'])
        self.assertEqual(service_user1_updated.instance.pk, data['instance'])
        self.assertEqual(service_user1_updated.username, data['username'])
        self.assertEqual(service_user1_updated.description, data['description'])
        self.assertEqual(service_user1_updated.note, data['note'])

    def test_delete_service_user(self):
        url = reverse('service-api:service-user-detail', kwargs={'pk': self.service_user1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ServiceUser.objects.count(), 2)


class ServiceManagerTest(APITestCase):

    def setUp(self):
        super().setUp()

        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.user1 = User.objects.create(username='testuser1')
        self.user2 = User.objects.create(username='testuser2')
        self.user3 = User.objects.create(username='testuser3')
        self.user4 = User.objects.create(username='testuser4')
        self.service1 = Service.objects.create(
            code='VCR_IT_IPCC_069',
            name='WEB THI ONLINE',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_ACTIVE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
            level_important=SERVICE_LEVEL_NORMAL,
            enduser=SERVICE_END_USER_VTNET,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VCR_IT_IPCC_069'
        )
        self.service2 = Service.objects.create(
            code='NAT_IT_BILL_004',
            name='Payment Process',
            manager=self.user2,
            service_group=self.service_group1,
            status=SERVICE_STATUS_DISABLE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='NAT_IT_BILL_004',
        )

        self.service_manager1 = ServiceManager.objects.create(
            service=self.service1,
            manager=self.user1,
            responsible=SERVICE_MANGER_RESPONSIBLE_PRIMARY,
            description='Service Manager 1 Test Description',
            note='Service Manager 1 Test Note'
        )
        self.service_manager2 = ServiceManager.objects.create(
            service=self.service2,
            manager=self.user2,
            responsible=SERVICE_MANGER_RESPONSIBLE_PRIMARY,
            description='Service Manager 2 Test Description',
            note='Service Manager 2 Test Note'
        )
        self.service_manager3 = ServiceManager.objects.create(
            service=self.service1,
            manager=self.user3,
            responsible=SERVICE_MANGER_RESPONSIBLE_SECONDARY,
            description='Service Manager 3 Test Description',
            note='Service Manager 3 Test Note'
        )
        self.service_manager4 = ServiceManager.objects.create(
            service=self.service2,
            manager=self.user1,
            responsible=SERVICE_MANGER_RESPONSIBLE_SECONDARY,
            description='Service Manager 4 Test Description',
            note='Service Manager 4 Test Note'
        )

    def test_list_service_manager(self):
        url = reverse('service-api:service-manager-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 4)

    def test_list_service_manager_filter_by_service_code(self):
        url = reverse('service-api:service-manager-list')
        response = self.client.get(url, {'service': self.service1.code}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

    def test_list_service_manager_filter_by_manager_username(self):
        url = reverse('service-api:service-manager-list')
        response = self.client.get(url, {'manager': self.user1.username}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

    def test_get_service_manager_detail(self):
        url = reverse('service-api:service-manager-detail', kwargs={'pk': self.service_manager1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.service_manager1.pk,
            'service': {
                'url': 'http://testserver/api/service/service/' + str(self.service1.pk) + '/',
                'id': self.service1.pk,
                'code': 'VCR_IT_IPCC_069',
                'name': 'WEB THI ONLINE',
                'status':
                    {
                        'value': 1,
                        'label': 'Active'
                    },
                'slug': 'VCR_IT_IPCC_069'
            },
            'manager': {
                'id': self.user1.pk,
                'username': 'testuser1',
                'first_name': '',
                'last_name': '',
                'email': ''
            },
            'responsible': {
                'value': 1,
                'label': 'Primary Responsible'
            },
            'description': 'Service Manager 1 Test Description',
            'note': 'Service Manager 1 Test Note'
        })

    def test_create_service_user(self):
        data = {
            'service': self.service1.pk,
            'manager': self.user4.pk,
            'responsible': SERVICE_MANGER_RESPONSIBLE_SECONDARY,
            'description': 'Service Manager X Test Description',
            'note': 'Service Manager X Test Note'
        }

        url = reverse('service-api:service-manager-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(ServiceManager.objects.count(), 5)

        service_manager_created = ServiceManager.objects.get(pk=response.data['id'])
        self.assertEqual(service_manager_created.service.pk, data['service'])
        self.assertEqual(service_manager_created.manager.pk, data['manager'])
        self.assertEqual(service_manager_created.responsible, data['responsible'])
        self.assertEqual(service_manager_created.description, data['description'])
        self.assertEqual(service_manager_created.note, data['note'])

    def test_update_service_manager(self):
        data = {
            'service': self.service1.pk,
            'manager': self.user4.pk,
            'responsible': SERVICE_MANGER_RESPONSIBLE_SECONDARY,
            'description': 'Service Manager X description',
            'note': 'Note service Manager x',
        }

        url = reverse('service-api:service-manager-detail', kwargs={'pk': self.service_manager2.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(ServiceManager.objects.count(), 4)

        service_manager2_updated = ServiceManager.objects.get(pk=self.service_manager2.pk)
        self.assertEqual(service_manager2_updated.service.pk, data['service'])
        self.assertEqual(service_manager2_updated.manager.pk, data['manager'])
        self.assertEqual(service_manager2_updated.responsible, data['responsible'])
        self.assertEqual(service_manager2_updated.description, data['description'])
        self.assertEqual(service_manager2_updated.note, data['note'])

    def test_delete_service_manager(self):
        url = reverse('service-api:service-manager-detail', kwargs={'pk': self.service_manager1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ServiceManager.objects.count(), 3)


class ServiceDocumentTest(APITestCase):

    def setUp(self):
        super().setUp()

        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.user1 = User.objects.create(username='testuser1')
        self.user2 = User.objects.create(username='testuser2')
        self.user3 = User.objects.create(username='testuser3')
        self.user4 = User.objects.create(username='testuser4')
        self.service1 = Service.objects.create(
            code='VCR_IT_IPCC_069',
            name='WEB THI ONLINE',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_ACTIVE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
            level_important=SERVICE_LEVEL_NORMAL,
            enduser=SERVICE_END_USER_VTNET,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VCR_IT_IPCC_069'
        )
        self.service2 = Service.objects.create(
            code='NAT_IT_BILL_004',
            name='Payment Process',
            manager=self.user2,
            service_group=self.service_group1,
            status=SERVICE_STATUS_DISABLE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='NAT_IT_BILL_004',
        )

        self.service_document1 = ServiceDocument.objects.create(
            service=self.service1,
            name='PL.02_Huong dan xu ly canh bao he thong VAS.doc',
            attach_file='1',
            path='3398/',
            file_name='PL.02_Huong dan xu ly canh bao he thong VAS.doc',
            description='Test Document 1',
            note='Test Document 1'
        )
        self.service_document2 = ServiceDocument.objects.create(
            service=self.service1,
            name='PL.03_Danh sach dieu kien dam bao.docx',
            attach_file='1',
            path='3399/',
            file_name='PL.03_Danh sach dieu kien dam bao.docx',
            description='Test Document 2',
            note='Test Document 2'
        )
        self.service_document3 = ServiceDocument.objects.create(
            service=self.service1,
            name='PL.04_HuongDanChung_SMS Block.docx',
            attach_file='1',
            path='3400/',
            file_name='PL.04_HuongDanChung_SMS Block.docx',
            description='Test Document 3',
            note='Test Document 3'
        )
        self.service_document4 = ServiceDocument.objects.create(
            service=self.service2,
            name='PL.05_Danh sach ung dung VAS.xlsx',
            attach_file='1',
            path='3401/',
            file_name='PL.05_Danh sach ung dung VAS.xlsx',
            description='Test Document 4',
            note='Test Document 4'
        )

    def test_list_service_document(self):
        url = reverse('service-api:service-document-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 4)

    def test_list_service_manager_filter_by_service_code(self):
        url = reverse('service-api:service-document-list')
        response = self.client.get(url, {'service': self.service1.code}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_get_service_document_detail(self):
        url = reverse('service-api:service-document-detail', kwargs={'pk': self.service_document1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.service_document1.pk,
            'service': {
                'url': 'http://testserver/api/service/service/' + str(self.service1.pk) + '/',
                'id': self.service1.pk,
                'code': 'VCR_IT_IPCC_069',
                'name': 'WEB THI ONLINE',
                'status': {
                    'value': 1,
                    'label': 'Active'
                },
                'slug': 'VCR_IT_IPCC_069'
            },
            'name': 'PL.02_Huong dan xu ly canh bao he thong VAS.doc',
            'attach_file': '1',
            'path': '3398/',
            'file_name': 'PL.02_Huong dan xu ly canh bao he thong VAS.doc',
            'description': 'Test Document 1',
            'note': 'Test Document 1'
        })

    def test_create_service_user(self):
        data = {
            'service': self.service1.pk,
            'name': 'PL.04_HuongDanChung_BusySMS.docx',
            'attach_file': '1',
            'path': '3353/',
            'file_name': 'PL.04_HuongDanChung_BusySMS.docx',
            'description': 'Test Document X',
            'note': 'Test Document X'
        }

        url = reverse('service-api:service-document-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(ServiceDocument.objects.count(), 5)

        service_document_created = ServiceDocument.objects.get(pk=response.data['id'])
        self.assertEqual(service_document_created.service.pk, data['service'])
        self.assertEqual(service_document_created.name, data['name'])
        self.assertEqual(service_document_created.attach_file, data['attach_file'])
        self.assertEqual(service_document_created.path, data['path'])
        self.assertEqual(service_document_created.file_name, data['file_name'])
        self.assertEqual(service_document_created.description, data['description'])
        self.assertEqual(service_document_created.note, data['note'])

    def test_update_service_manager(self):
        data = {
            'service': self.service2.pk,
            'name': 'PL.04_HuongDanChung_BusySMS.docx',
            'attach_file': '1',
            'path': '3353/',
            'file_name': 'PL.04_HuongDanChung_BusySMS.docx',
            'description': 'Test Document X',
            'note': 'Test Document X'
        }

        url = reverse('service-api:service-document-detail', kwargs={'pk': self.service_document1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(ServiceDocument.objects.count(), 4)

        service_document1_updated = ServiceDocument.objects.get(pk=self.service_document1.pk)
        self.assertEqual(service_document1_updated.service.pk, data['service'])
        self.assertEqual(service_document1_updated.name, data['name'])
        self.assertEqual(service_document1_updated.attach_file, data['attach_file'])
        self.assertEqual(service_document1_updated.path, data['path'])
        self.assertEqual(service_document1_updated.file_name, data['file_name'])
        self.assertEqual(service_document1_updated.description, data['description'])
        self.assertEqual(service_document1_updated.note, data['note'])

    def test_delete_service_manager(self):
        url = reverse('service-api:service-document-detail', kwargs={'pk': self.service_document1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ServiceDocument.objects.count(), 3)


class ServiceLicenseTest(APITestCase):

    def setUp(self):
        super().setUp()

        self.tenant1 = Tenant.objects.create(name='Test Tenant 1', slug='test-tenant-1')
        self.service_group1 = ServiceGroup.objects.create(
            code='DATAMON', name='DATAMON', tenant=self.tenant1, status=SERVICE_GROUP_ACTIVE,
            description='DATAMON SYSTEM', slug='datamon'
        )
        self.user1 = User.objects.create(username='testuser1')
        self.user2 = User.objects.create(username='testuser2')
        self.user3 = User.objects.create(username='testuser3')
        self.user4 = User.objects.create(username='testuser4')
        self.service1 = Service.objects.create(
            code='VCR_IT_IPCC_069',
            name='WEB THI ONLINE',
            manager=self.user1,
            service_group=self.service_group1,
            status=SERVICE_STATUS_ACTIVE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_ACTIVE,
            level_important=SERVICE_LEVEL_NORMAL,
            enduser=SERVICE_END_USER_VTNET,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='VCR_IT_IPCC_069'
        )
        self.service2 = Service.objects.create(
            code='NAT_IT_BILL_004',
            name='Payment Process',
            manager=self.user2,
            service_group=self.service_group1,
            status=SERVICE_STATUS_DISABLE,
            tenant=self.tenant1,
            backup_type=SERVICE_BACKUP_TYPE_ACTIVE_STANDBY,
            level_important=SERVICE_LEVEL_IMPORTANT,
            enduser=SERVICE_END_USER_EXTERNAL_VIETTEL,
            customer=SERVICE_CUSTOMER_ENTERPRISE,
            user_influence=SERVICE_USER_INFLUENCE_DIRECT,
            kpi=SERVICE_KPI_METHOD_ADD_ON,
            verify=SERVICE_VERIFY_VERIFY,
            monitored=SERVICE_MONITOR__STATUS_MONITORED,
            dev_unit=SERVICE_DEV_UNIT_1,
            dedicated=SERVICE_DEDICATED_DEDICATED,
            slug='NAT_IT_BILL_004',
        )

        self.service_license1 = ServiceLicense.objects.create(
            service=self.service1,
            license='Test Service License 1',
            type=SERVICE_LICENSE_TYPE_APACHE,
            value='Test Service License 1',
            start_time=parse_datetime('2016-10-03T19:00:00Z'),
            expire_time=parse_datetime('2020-10-03T19:00:00Z'),
            alarm_time=parse_datetime('2019-10-03T19:00:00Z'),
            description='Test Service License 1 Description',
            note='Test Service License 1 Note'
        )
        self.service_license2 = ServiceLicense.objects.create(
            service=self.service1,
            license='Test Service License 2',
            type=SERVICE_LICENSE_TYPE_MIT,
            value='Test Service License 2',
            start_time=parse_datetime('2016-10-10T19:00:00Z'),
            expire_time=parse_datetime('2020-10-10T19:00:00Z'),
            alarm_time=parse_datetime('2019-10-10T19:00:00Z'),
            description='Test Service License 2 Description',
            note='Test Service License 2 Note'
        )
        self.service_license3 = ServiceLicense.objects.create(
            service=self.service2,
            license='Test Service License 3',
            type=SERVICE_LICENSE_TYPE_APACHE,
            value='Test Service License 3',
            start_time=parse_datetime('2016-10-17T19:00:00Z'),
            expire_time=parse_datetime('2020-10-18T19:00:00Z'),
            alarm_time=parse_datetime('2019-10-19T19:00:00Z'),
            description='Test Service License 3 Description',
            note='Test Service License 3 Note'
        )

    def test_list_service_license(self):
        url = reverse('service-api:service-license-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_list_service_license_filter_by_service_code(self):
        url = reverse('service-api:service-license-list')
        response = self.client.get(url, {'service': self.service1.code}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

    def test_get_service_license_detail(self):
        url = reverse('service-api:service-license-detail', kwargs={'pk': self.service_license1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.service_license1.pk,
            'service': {
                'url': 'http://testserver/api/service/service/' + str(self.service1.pk) + '/',
                'id': self.service1.pk,
                'code': 'VCR_IT_IPCC_069',
                'name': 'WEB THI ONLINE',
                'status': {
                    'value': 1,
                    'label': 'Active'
                },
                'slug': 'VCR_IT_IPCC_069'
            },
            'license': 'Test Service License 1',
            'type': {
                'value': 2,
                'label': 'Apache License'
            },
            'value': 'Test Service License 1',
            'start_time': '2016-10-03T19:00:00Z',
            'expire_time': '2020-10-03T19:00:00Z',
            'alarm_time': '2019-10-03T19:00:00Z',
            'description': 'Test Service License 1 Description',
            'note': 'Test Service License 1 Note'
        })

    def test_create_service_license(self):
        data = {
            'service': self.service2.pk,
            'license': 'Test Service License X',
            'type': SERVICE_LICENSE_TYPE_APACHE,
            'value': 'Test Service License X',
            'start_time': '2018-10-03T19:00:00Z',
            'expire_time': '2022-10-03T19:00:00Z',
            'alarm_time': '2021-10-03T19:00:00Z',
            'description': 'Test Service License X Description',
            'note': 'Test Service License X Note'
        }

        url = reverse('service-api:service-license-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(ServiceLicense.objects.count(), 4)

        service_license_created = ServiceLicense.objects.get(pk=response.data['id'])
        self.assertEqual(service_license_created.service.pk, data['service'])
        self.assertEqual(service_license_created.license, data['license'])
        self.assertEqual(service_license_created.type, data['type'])
        self.assertEqual(service_license_created.value, data['value'])
        self.assertEqual(service_license_created.start_time, parse_datetime(data['start_time']))
        self.assertEqual(service_license_created.expire_time, parse_datetime(data['expire_time']))
        self.assertEqual(service_license_created.alarm_time, parse_datetime(data['alarm_time']))
        self.assertEqual(service_license_created.description, data['description'])
        self.assertEqual(service_license_created.note, data['note'])

    def test_update_service_license(self):
        data = {
            'service': self.service2.pk,
            'license': 'Test Service License X',
            'type': SERVICE_LICENSE_TYPE_APACHE,
            'value': 'Test Service License X',
            'start_time': '2018-10-03T19:00:00Z',
            'expire_time': '2022-10-03T19:00:00Z',
            'alarm_time': '2021-10-03T19:00:00Z',
            'description': 'Test Service License X Description',
            'note': 'Test Service License X Note'
        }

        url = reverse('service-api:service-license-detail', kwargs={'pk': self.service_license1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(ServiceLicense.objects.count(), 3)

        service_license1_updated = ServiceLicense.objects.get(pk=self.service_license1.pk)
        self.assertEqual(service_license1_updated.service.pk, data['service'])
        self.assertEqual(service_license1_updated.license, data['license'])
        self.assertEqual(service_license1_updated.type, data['type'])
        self.assertEqual(service_license1_updated.value, data['value'])
        self.assertEqual(service_license1_updated.start_time, parse_datetime(data['start_time']))
        self.assertEqual(service_license1_updated.expire_time, parse_datetime(data['expire_time']))
        self.assertEqual(service_license1_updated.alarm_time, parse_datetime(data['alarm_time']))
        self.assertEqual(service_license1_updated.description, data['description'])
        self.assertEqual(service_license1_updated.note, data['note'])

    def test_delete_service_license(self):
        url = reverse('service-api:service-license-detail', kwargs={'pk': self.service_license1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ServiceLicense.objects.count(), 2)


class ModuleGroupTest(ServiceAPITest):

    def setUp(self):
        super().setUp()

    def test_get_module_group_list(self):
        url = reverse('service-api:module-group-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 4)

    def test_get_module_group_list_filter_by_status(self):
        url = reverse('service-api:module-group-list')
        response = self.client.get(url, {'status': MODULE_GROUP_STATUS_ACTIVE}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_get_module_group_detail(self):
        url = reverse('service-api:module-group-detail', kwargs={'pk': self.module_group1.pk})
        response = self.client.get(url, **self.header)
        self.assertHttpStatus(response, status.HTTP_200_OK)

        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.module_group1.pk,
            'code': 'test-module-group-1',
            'name': 'Test Module Group 1',
            'status': {
                'value': 1,
                'label': 'Active'
            },
            'description': 'Test Module Group 1 Description',
            'slug': 'test-module-group-1',
            'tags': []
        })

    def test_create_module_group(self):
        pass
        data = {
            'code': 'test-module-group-x',
            'name': 'Test Module Group X',
            'status': MODULE_GROUP_STATUS_DEACTIVE,
            'description': 'Test Module Group X Description',
            'slug': 'test-module-group-x',
        }

        url = reverse('service-api:module-group-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(ModuleGroup.objects.count(), 5)

        module_group_created = ModuleGroup.objects.get(pk=response.data['id'])
        self.assertEqual(module_group_created.code, data['code'])
        self.assertEqual(module_group_created.name, data['name'])
        self.assertEqual(module_group_created.status, data['status'])
        self.assertEqual(module_group_created.description, data['description'])
        self.assertEqual(module_group_created.slug, data['slug'])

    def test_update_module_group(self):
        pass
        data = {
            'code': 'test-module-group-x',
            'name': 'Test Module Group X',
            'status': MODULE_GROUP_STATUS_DEACTIVE,
            'description': 'Test Module Group X Description',
            'slug': 'test-module-group-x',
        }

        url = reverse('service-api:module-group-detail', kwargs={'pk': self.module_group1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(ModuleGroup.objects.count(), 4)

        module_group1_updated = ModuleGroup.objects.get(pk=response.data['id'])
        self.assertEqual(module_group1_updated.code, data['code'])
        self.assertEqual(module_group1_updated.name, data['name'])
        self.assertEqual(module_group1_updated.status, data['status'])
        self.assertEqual(module_group1_updated.description, data['description'])
        self.assertEqual(module_group1_updated.slug, data['slug'])

    def test_delete_module_group_fail_protected(self):
        url = reverse('service-api:module-group-detail', kwargs={'pk': self.module_group1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_423_LOCKED)
        self.assertEqual(ModuleGroup.objects.count(), 4)

    def test_delete_module_group(self):
        url = reverse('service-api:module-group-detail', kwargs={'pk': self.module_group4.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ModuleGroup.objects.count(), 3)


class ModuleTest(ServiceAPITest):

    def setUp(self):
        super().setUp()

    def test_get_module_list(self):
        url = reverse('service-api:module-list')
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

    def test_get_module_list_filter_by_status(self):
        url = reverse('service-api:module-list')
        response = self.client.get(url, {'status': MODULE_STATUS_RUNNING}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

    def test_get_module_list_filter_by_service_id(self):
        url = reverse('service-api:module-list')
        response = self.client.get(url, {'service_id': self.service2.pk}, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)

    def test_get_module_detail(self):
        url = reverse('service-api:module-detail', kwargs={'pk': self.module1.pk})
        response = self.client.get(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        response_json = json.loads(response.content)
        self.assertEqual(response_json, {
            'id': self.module1.pk,
            'service': {
                'url': 'http://testserver/api/service/service/' + str(self.module1.service.pk) + '/',
                'id': self.module1.service.pk,
                'code': 'VCR_IT_IPCC_069',
                'name': 'WEB THI ONLINE',
                'status': {
                    'value': 1,
                    'label': 'Active'
                },
                'slug': 'VCR_IT_IPCC_069'
            },
            'group': {
                'url': 'http://testserver/api/service/module-group/' + str(self.module1.group.pk) + '/',
                'id': self.module1.group.pk,
                'code': 'test-module-group-1',
                'name': 'Test Module Group 1',
                'status': {
                    'value': 1,
                    'label': 'Active'
                },
                'slug': 'test-module-group-1'
            },
            'code': 'test_module_1',
            'name': 'Test Module 1',
            'type': {
                'value': 19,
                'label': 'Apache Kafka'
            },
            'service_user': {
                'url': 'http://testserver/api/service/service-user/' + str(self.module1.service_user.pk) + '/',
                'id': self.module1.service_user.pk,
                'service': {
                    'url': 'http://testserver/api/service/service/' + str(self.module1.service.pk) + '/',
                    'id': self.module1.service.pk,
                    'code': 'VCR_IT_IPCC_069',
                    'name': 'WEB THI ONLINE',
                    'status': {
                        'value': 1,
                        'label': 'Active'
                    },
                    'slug': 'VCR_IT_IPCC_069'
                },
                'instance': {
                    'id': self.module1.service_user.instance.pk,
                    'url': 'http://testserver/api/dcim/instances/' + str(self.module1.service_user.instance.pk) + '/',
                    'name': 'Instance 1'
                },
                'username': 'app1'
            },
            'module_function': 'Test Module 1 Function',
            'pro_language': {
                'value': 5,
                'label': 'Java'
            },
            'web_server': {
                'value': 4,
                'label': 'Nginx'
            },
            'status': {
                'value': 1,
                'label': 'Running'
            },
            'real_status': {
                'value': 1,
                'label': 'ON'
            },
            'port_mm': 8086,
            'uptime': '3-06:06:36',
            'purpose': 'Test Module 1 Purpose',
            'description': 'Test Module 1 Description',
            'stop_date': None,
            'reason_deactive': '',
            'slug': 'test-module-1',
            'tags': []
        })

    def test_create_module(self):
        data = {
            'service': self.service1.pk,
            'group': self.module_group1.pk,
            'code': 'test_module_x',
            'name': 'Test Module X',
            'type': MODULE_TYPE_JAVA_PROCESS,
            'service_user': self.service_user4.pk,
            'module_function': 'Test Module X Function',
            'pro_language': MODULE_PROGRAMMING_LANGUAGE_JAVA,
            'web_server': MODULE_WEB_SERVER_Nginx,
            'status': MODULE_STATUS_RUNNING,
            'real_status': MODULE_REAL_STATUS_ON,
            'uptime': '3-06:06:36',
            'purpose': 'Test Module 1 Purpose',
            'description': 'Module X Test Description',
            'slug': 'test-module-x'
        }

        url = reverse('service-api:module-list')
        response = self.client.post(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_201_CREATED)
        self.assertEqual(Module.objects.count(), 4)

        module_created = Module.objects.get(pk=response.data['id'])
        self.assertEqual(module_created.service.pk, data['service'])
        self.assertEqual(module_created.group.pk, data['group'])
        self.assertEqual(module_created.code, data['code'])
        self.assertEqual(module_created.name, data['name'])
        self.assertEqual(module_created.type, data['type'])
        self.assertEqual(module_created.service_user.pk, data['service_user'])
        self.assertEqual(module_created.module_function, data['module_function'])
        self.assertEqual(module_created.pro_language, data['pro_language'])
        self.assertEqual(module_created.web_server, data['web_server'])
        self.assertEqual(module_created.status, data['status'])
        self.assertEqual(module_created.real_status, data['real_status'])
        self.assertEqual(module_created.uptime, data['uptime'])
        self.assertEqual(module_created.purpose, data['purpose'])
        self.assertEqual(module_created.description, data['description'])
        self.assertEqual(module_created.slug, data['slug'])

    def test_update_module(self):
        data = {
            'service': self.service1.pk,
            'group': self.module_group1.pk,
            'code': 'test_module_x',
            'name': 'Test Module X',
            'type': MODULE_TYPE_JAVA_PROCESS,
            'service_user': self.service_user1.pk,
            'module_function': 'Test Module X Function',
            'pro_language': MODULE_PROGRAMMING_LANGUAGE_JAVA,
            'web_server': MODULE_WEB_SERVER_Nginx,
            'status': MODULE_STATUS_RUNNING,
            'real_status': MODULE_REAL_STATUS_ON,
            'uptime': '3-06:06:36',
            'purpose': 'Test Module 1 Purpose',
            'description': 'Module X Test Description',
            'slug': 'test-module-x'
        }

        url = reverse('service-api:module-detail', kwargs={'pk': self.module1.pk})
        response = self.client.put(url, data, format='json', **self.header)

        self.assertHttpStatus(response, status.HTTP_200_OK)
        self.assertEqual(Module.objects.count(), 3)

        module1_updated = Module.objects.get(pk=self.module1.pk)
        self.assertEqual(module1_updated.service.pk, data['service'])
        self.assertEqual(module1_updated.group.pk, data['group'])
        self.assertEqual(module1_updated.code, data['code'])
        self.assertEqual(module1_updated.name, data['name'])
        self.assertEqual(module1_updated.type, data['type'])
        self.assertEqual(module1_updated.service_user.pk, data['service_user'])
        self.assertEqual(module1_updated.module_function, data['module_function'])
        self.assertEqual(module1_updated.pro_language, data['pro_language'])
        self.assertEqual(module1_updated.web_server, data['web_server'])
        self.assertEqual(module1_updated.status, data['status'])
        self.assertEqual(module1_updated.real_status, data['real_status'])
        self.assertEqual(module1_updated.uptime, data['uptime'])
        self.assertEqual(module1_updated.purpose, data['purpose'])
        self.assertEqual(module1_updated.description, data['description'])
        self.assertEqual(module1_updated.slug, data['slug'])

    def test_delete_module(self):
        url = reverse('service-api:module-detail', kwargs={'pk': self.module1.pk})
        response = self.client.delete(url, **self.header)

        self.assertHttpStatus(response, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Module.objects.count(), 2)
