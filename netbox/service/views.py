from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import transaction
from django.db.models import Count
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views.generic import View

from extras.models import Graph, TopologyMap, GRAPH_TYPE_INTERFACE, GRAPH_TYPE_SITE
from extras.views import ObjectConfigContextView


from utilities.forms import ConfirmationForm
from utilities.paginator import EnhancedPaginator
from utilities.utils import csv_format
from utilities.views import (
    BulkComponentCreateView, BulkDeleteView, BulkEditView, BulkImportView, ComponentCreateView, GetReturnURLMixin,
    ObjectDeleteView, ObjectEditView, ObjectListView,
)
from dcim.models import Instance
from tenancy.models import Tenant

from . import filters, forms, tables
from .models import (
    Service, ServiceGroup, ServiceUser, ServiceContact, ServiceManager, ServiceDocument, ServiceLicense
)


# 
# Service group
# 


class ServiceGroupListView(ObjectListView):
    queryset = ServiceGroup.objects.annotate(service_count = Count('services'))
    table = tables.ServiceGroupTable
    template_name = 'service/servicegroup_list.html'

class ServiceGroupCreateView(PermissionRequiredMixin, ObjectEditView):
    permission_required = 'service.add_servicegroup'
    model = ServiceGroup
    model_form = forms.ServiceGroupForm
    default_return_url = 'service:servicegroup_list'


class ServiceGroupEditView(ServiceGroupCreateView):
    permission_required = 'service.change_servicegroup'


class ServiceGroupBulkImportView(PermissionRequiredMixin, BulkImportView):
    permission_required = 'sevice.add_servicegroup'
    model_form = forms.ServiceGroupCSVForm
    table = tables.ServiceGroupTable
    default_return_url = 'service:servicegroup_list'


class ServiceGroupBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    permission_required = 'sevice.delete_servicegroup'
    queryset = ServiceGroup.objects.annotate(service_count=Count('services'))
    table = tables.ServiceGroupTable
    default_return_url = 'service:servicegroup_list'


# 
# Service
# 


class ServiceListView(ObjectListView):
    queryset = Service.objects.select_related(
        'manager', 'service_group', 'tenant',
    )
    filter = filters.ServiceFilter
    filter_form = forms.ServiceFilterForm
    table = tables.ServiceTable
    template_name = 'service/service_list.html'

class ServiceView(View):
    def get(self, request, pk):
        service = get_object_or_404(Service.objects.select_related(
            'manager', 'tenant','user_name', 'service_group'
        ), pk=pk)

        username = service.serviceuser.select_related('serviceuser')

        return render(request, 'service/service.html', {
            'service': service,
        
        })


class ServiceConfigContextView(ObjectConfigContextView):
    object_class = Service
    base_template = 'service/service.html'


class ServiceCreateView(PermissionRequiredMixin, ObjectEditView):
    permission_required = 'service.add_servicegroup'
    model = Service
    model_form = forms.ServiceForm
    template_name = 'service/service_edit.html'
    default_return_url = 'service:service_list'


class ServiceEditView(ServiceCreateView):
    permission_required = 'service.change_service'


class ServiceDeleteView(PermissionRequiredMixin, ObjectDeleteView):
    permission_required = 'service.delete_service'
    model = Service
    default_return_url = 'service:service_list'


class ServiceBulkImportView(PermissionRequiredMixin, BulkImportView):
    permission_required = 'service.add_service'
    model_form = forms.ServiceCSVForm
    table = tables.ServiceImportTable
    template_name = 'service/service_import.html'
    default_return_url = 'service:service_list'


class ServiceBulkEditView(PermissionRequiredMixin, BulkEditView):
    permission_required = 'service.change_service'
    queryset = Service.objects.select_related(
        'manager', 'service_group', 'tenant'
    )
    filter = filters.ServiceFilter
    table = tables.ServiceTable
    form = forms.ServiceBulkEditForm
    default_return_url = 'service:service_list'


class ServiceBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    permission_required = 'service.delete_service'
    queryset = Service.objects.select_related(
        'manager', 'service_group', 'tenant'
    )
    filter = filters.ServiceFilter
    table = tables.ServiceTable
    default_return_url = 'service:service_list'


# Service user


class ServiceUserListView(ObjectListView):
    
    queryset = Service.objects.select_related()
    table = tables.ServiceUserTable
    template_name = 'service/serviceuser_list.html'

class ServiceUserCreateView(PermissionRequiredMixin, ObjectEditView):
    permission_required = 'service.add_serviceuser'
    model = ServiceUser
    model_form = forms.ServiceUserForm
    default_return_url = 'service:serviceuser_list'


class ServiceUserEditView(ServiceUserCreateView):
    permission_required = 'service.change_serviceuser'


class ServiceUserBulkImportView(PermissionRequiredMixin, BulkImportView):
    permission_required = 'sevice.add_serviceuser'
    model_form = forms.ServiceUserCSVForm
    table = tables.ServiceUserTable
    default_return_url = 'service:serviceuser_list'


class ServiceUserBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    permission_required = 'sevice.delete_serviceuser'
    queryset = ServiceUser.objects.annotate(service_count=Count('service'))
    table = tables.ServiceUserTable
    default_return_url = 'service:serviceuser_list'

# 
# Service contact
# 

class ServiceContactListView(ObjectListView):
    queryset = ServiceContact.objects.annotate(service_count = Count('service'))
    table = tables.ServiceContactTable
    template_name = 'service/servicecontact_list.html'

class ServiceContactCreateView(PermissionRequiredMixin, ObjectEditView):
    permission_required = 'service.add_servicecontact'
    model = ServiceContact
    model_form = forms.ServiceContactForm
    default_return_url = 'service:servicecontact_list'


class ServiceContactEditView(ServiceContactCreateView):
    permission_required = 'service.change_servicecontact'


class ServiceContactBulkImportView(PermissionRequiredMixin, BulkImportView):
    permission_required = 'sevice.add_servicecontact'
    model_form = forms.ServiceContactCSVForm
    table = tables.ServiceContactTable
    default_return_url = 'service:servicecontact_list'


class ServiceContactBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    permission_required = 'sevice.delete_servicecontact'
    queryset = ServiceContact.objects.annotate(service_count=Count('service'))
    table = tables.ServiceContactTable
    default_return_url = 'service:servicecontact_list'

# 
# Service document
# 

class ServiceDocumentListView(ObjectListView):
    queryset = ServiceDocument.objects.annotate()
    table = tables.ServiceDocumentTable
    template_name = 'service/servicedocument_list.html'

class ServiceDocumentCreateView(PermissionRequiredMixin, ObjectEditView):
    permission_required = 'service.add_servicedocument'
    model = ServiceDocument
    model_form = forms.ServiceDocumentForm
    default_return_url = 'service:servicedocument_list'


class ServiceDocumentEditView(ServiceDocumentCreateView):
    permission_required = 'service.change_servicedocument'


class ServiceDocumentBulkImportView(PermissionRequiredMixin, BulkImportView):
    permission_required = 'sevice.add_servicedocument'
    model_form = forms.ServiceDocumentCSVForm
    table = tables.ServiceDocumentTable
    default_return_url = 'service:servicedocument_list'


class ServiceDocumentBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    permission_required = 'sevice.delete_servicedocument'
    queryset = ServiceDocument.objects.annotate()
    table = tables.ServiceDocumentTable
    default_return_url = 'service:servicedocument_list'

# 
# Service license
# 

class ServiceLicenseListView(ObjectListView):
    queryset = ServiceLicense.objects.annotate()
    table = tables.ServiceLicenseTable
    template_name = 'service/servicelicense_list.html'

class ServiceLicenseCreateView(PermissionRequiredMixin, ObjectEditView):
    permission_required = 'service.add_servicelicense'
    model = ServiceLicense
    model_form = forms.ServiceLicenseForm
    default_return_url = 'service:servicelicense_list'


class ServiceLicenseEditView(ServiceLicenseCreateView):
    permission_required = 'service.change_servicelicense'


class ServiceLicenseBulkImportView(PermissionRequiredMixin, BulkImportView):
    permission_required = 'sevice.add_servicelicense'
    model_form = forms.ServiceLicenseCSVForm
    table = tables.ServiceLicenseTable
    default_return_url = 'service:servicelicense_list'


class ServiceLicenseBulkDeleteView(PermissionRequiredMixin, BulkDeleteView):
    permission_required = 'sevice.delete_servicelicense'
    queryset = ServiceLicense.objects.annotate()
    table = tables.ServiceLicenseTable
    default_return_url = 'service:servicelicense_list'

