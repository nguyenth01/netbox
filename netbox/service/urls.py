from django.conf.urls import url

from extras.views import ObjectChangeLogView
from . import views
from .models import ServiceGroup, Service, ServiceUser, ServiceContact, ServiceDocument, ServiceLicense

app_name = 'service'

urlpatterns = [
    # service group
    url(r'^service-groups/$', views.ServiceGroupListView.as_view(), name='servicegroup_list'),
    url(r'^service-groups/add/$', views.ServiceGroupCreateView.as_view(), name='servicegroup_add'),
    url(r'^service-groups/import/$', views.ServiceGroupBulkImportView.as_view(), name='servicegroup_import'),
    url(r'^service-groups/delete/$', views.ServiceGroupBulkDeleteView.as_view(), name='servicegroup_bulk_delete'),
    url(r'^service-groups/(?P<slug>[\w-]+)/edit/$', views.ServiceGroupEditView.as_view(), name='servicegroup_edit'),
    url(r'^service-groups/(?P<slug>[\w-]+)/changelog/$', ObjectChangeLogView.as_view(), name='servicegroup_changelog', kwargs={'model': ServiceGroup}),
    
    # Service
    url(r'^services/$', views.ServiceListView.as_view(), name='service_list'),
    url(r'^services/add/$', views.ServiceCreateView.as_view(), name='service_add'),
    url(r'^services/import/$', views.ServiceBulkImportView.as_view(), name='service_import'),
    url(r'^services/(?P<pk>\d+)/$', views.ServiceView.as_view(), name='service'),
    url(r'^services/(?P<pk>\d+)/edit/$', views.ServiceEditView.as_view(), name='service_edit'),
    url(r'^services/(?P<pk>\d+)/delete/$', views.ServiceDeleteView.as_view(), name='service_delete'),
    url(r'^service/edit/$', views.ServiceBulkEditView.as_view(), name='service_bulk_edit'),
    url(r'^service/delete/$', views.ServiceBulkDeleteView.as_view(), name='service_bulk_delete'),
    
    # Service user
    url(r'^service-users/$', views.ServiceUserListView.as_view(), name='serviceuser_list'),
    url(r'^service-users/add/$', views.ServiceUserCreateView.as_view(), name='serviceuser_add'),
    url(r'^service-users/import/$', views.ServiceUserBulkImportView.as_view(), name='serviceuser_import'),
    url(r'^service-users/delete/$', views.ServiceUserBulkDeleteView.as_view(), name='serviceuser_bulk_delete'),
    url(r'^service-users/(?P<pk>[\w-]+)/edit/$', views.ServiceUserEditView.as_view(), name='serviceuser_edit'),
    url(r'^service-users/(?P<pk>[\w-]+)/changelog/$', ObjectChangeLogView.as_view(), name='serviceuser_changelog', kwargs={'model': ServiceUser}),
    
    # Service contact
    url(r'^service-contacts/$', views.ServiceContactListView.as_view(), name='servicecontact_list'),
    url(r'^service-contacts/add/$', views.ServiceContactCreateView.as_view(), name='servicecontact_add'),
    url(r'^service-contacts/import/$', views.ServiceContactBulkImportView.as_view(), name='servicecontact_import'),
    url(r'^service-contacts/delete/$', views.ServiceContactBulkDeleteView.as_view(), name='servicecontact_bulk_delete'),
    url(r'^service-contacts/(?P<pk>[\w-]+)/edit/$', views.ServiceContactEditView.as_view(), name='servicecontact_edit'),
    url(r'^service-contacts/(?P<pk>[\w-]+)/changelog/$', ObjectChangeLogView.as_view(), name='servicecontact_changelog', kwargs={'model': ServiceContact}),
    
    # Service document
    url(r'^service-documents/$', views.ServiceDocumentListView.as_view(), name='servicedocument_list'),
    url(r'^service-documents/add/$', views.ServiceDocumentCreateView.as_view(), name='servicedocument_add'),
    url(r'^service-documents/import/$', views.ServiceDocumentBulkImportView.as_view(), name='servicedocument_import'),
    url(r'^service-documents/delete/$', views.ServiceDocumentBulkDeleteView.as_view(), name='servicedocument_bulk_delete'),
    url(r'^service-documents/(?P<pk>[\w-]+)/edit/$', views.ServiceDocumentEditView.as_view(), name='servicedocument_edit'),
    url(r'^service-documents/(?P<pk>[\w-]+)/changelog/$', ObjectChangeLogView.as_view(), name='servicedocument_changelog', kwargs={'model': ServiceDocument}),

    # Service license
    url(r'^service-licenses/$', views.ServiceLicenseListView.as_view(), name='servicelicense_list'),
    url(r'^service-licenses/add/$', views.ServiceLicenseCreateView.as_view(), name='servicelicense_add'),
    url(r'^service-licenses/import/$', views.ServiceLicenseBulkImportView.as_view(), name='servicelicense_import'),
    url(r'^service-licenses/delete/$', views.ServiceLicenseBulkDeleteView.as_view(), name='servicelicense_bulk_delete'),
    url(r'^service-licenses/(?P<pk>[\w-]+)/edit/$', views.ServiceLicenseEditView.as_view(), name='servicelicense_edit'),
    url(r'^service-licenses/(?P<pk>[\w-]+)/changelog/$', ObjectChangeLogView.as_view(), name='servicelicense_changelog', kwargs={'model': ServiceLicense}),

]
