from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from taggit.managers import TaggableManager
from django.urls import reverse
from extras.models import CustomFieldModel
from utilities.api import convert_choices_to_str
from utilities.models import ChangeLoggedModel
from .constants import *


class ServiceGroup(ChangeLoggedModel):
    """
    A Service Group represents a group of services.
    """
    code = models.CharField(
        max_length=255,
        unique=True
    )
    name = models.CharField(
        max_length=255,
        unique=True
    )
    status = models.PositiveSmallIntegerField(
        choices=SERVICE_GROUP_STATUS_CHOICES,
        help_text='Service Group Statuses: 0 - Deactive; 1 - Active'
    )
    tenant = models.ForeignKey(
        to='tenancy.Tenant',
        on_delete=models.PROTECT,
        related_name='service_groups',
    )
    description = models.CharField(
        max_length=500,
        blank=True,
        help_text='Long-form name (optional)'
    )
    slug = models.SlugField(
        unique=True
    )
    note = models.CharField(
        max_length=255,
        blank=True,
        help_text='Service Group note'
    )
     
    csv_headers = ['code', 'name','status','tenant', 'slug']

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


    # csv export

    def to_csv(self):
        return (
            self.name,
            self.slug,
            
        )


class Service(ChangeLoggedModel):
    """
    A Service represents a application service.
    """
    code = models.CharField(
        max_length=255,
        unique=True
    )
    name = models.CharField(
        max_length=255,
        unique=True
    )
    manager = models.ForeignKey(
        to='auth.User',
        on_delete=models.PROTECT,
        related_name='services'
    )
    service_group = models.ForeignKey(
        to='ServiceGroup',
        on_delete=models.PROTECT,
        related_name='services',
        blank=True,
        null=True
    )
    status = models.PositiveSmallIntegerField(
        choices=SERVICE_STATUS_CHOICES,
        help_text='Service Statuses: ' + convert_choices_to_str(SERVICE_STATUS_CHOICES)
    )
    start_date = models.DateTimeField(
        null=True
    )
    stop_date = models.DateTimeField(
        null=True
    )
    tenant = models.ForeignKey(
        to='tenancy.Tenant',
        on_delete=models.PROTECT,
        related_name='services',
    )
    backup_type = models.PositiveSmallIntegerField(
        choices=SERVICE_BACKUP_TYPE_CHOICES,
        help_text='Service Backup Type: ' + convert_choices_to_str(SERVICE_BACKUP_TYPE_CHOICES)
    )
    level_important = models.PositiveSmallIntegerField(
        choices=SERVICE_LEVEL_IMPORTANT_CHOICES,
        help_text='Service Level Important: ' + convert_choices_to_str(SERVICE_LEVEL_IMPORTANT_CHOICES)
    )
    enduser = models.PositiveSmallIntegerField(
        choices=SERVICE_END_USER_CHOICES,
        help_text='Service End User: ' + convert_choices_to_str(SERVICE_END_USER_CHOICES)
    )
    customer = models.PositiveSmallIntegerField(
        choices=SERVICE_CUSTOMER_CHOICES,
        help_text='Service Customer: ' + convert_choices_to_str(SERVICE_CUSTOMER_CHOICES)
    )
    user_influence = models.PositiveSmallIntegerField(
        choices=SERVICE_USER_INFLUENCE_CHOICES,
        help_text='User Influence: ' + convert_choices_to_str(SERVICE_USER_INFLUENCE_CHOICES)
    )
    kpi = models.PositiveSmallIntegerField(
        choices=SERVICE_KPI_METHOD_CHOICES,
        help_text='Service KPI: ' + convert_choices_to_str(SERVICE_KPI_METHOD_CHOICES)
    )
    peak_time = models.CharField(
        max_length=255,
        blank=True,
        help_text='Service peak time'
    )
    low_time = models.CharField(
        max_length=255,
        blank=True,
        help_text='Service low time'
    )
    max_exchange = models.CharField(
        max_length=1000,
        blank=True,
        help_text='Service max exchange'
    )
    avg_exchange = models.CharField(
        max_length=1000,
        blank=True,
        help_text='Service avg exchange'
    )
    purpose = models.CharField(
        max_length=1000,
        blank=True
    )
    description = models.CharField(
        max_length=3000,
        blank=True,
    )
    reason_deactive = models.CharField(
        max_length=1000,
        blank=True,
    )
    verify = models.PositiveSmallIntegerField(
        choices=SERVICE_VERIFY_CHOICES,
        help_text='Service Verify: ' + convert_choices_to_str(SERVICE_VERIFY_CHOICES)
    )
    monitored = models.PositiveSmallIntegerField(
        choices=SERVICE_MONITOR_STATUS_CHOICES,
        help_text='Service Monitor Statuses: ' + convert_choices_to_str(SERVICE_MONITOR_STATUS_CHOICES)
    )
    # TODO missing dev_unit dedicated fields choices
    dev_unit = models.PositiveSmallIntegerField(
        choices=SERVICE_DEV_UNIT_CHOICES,
        help_text='Service Development Unit: ' + convert_choices_to_str(SERVICE_DEV_UNIT_CHOICES)
    )
    dedicated = models.PositiveSmallIntegerField(
        choices=SERVICE_DEDICATED_CHOICES,
        help_text='Service Development Unit: ' + convert_choices_to_str(SERVICE_DEDICATED_CHOICES)
    )
    reason_not_monitored = models.CharField(
        max_length=500,
        blank=True,
    )
    monitored_off_time = models.DateTimeField(
        null=True
    )
    slug = models.SlugField(
        unique=True
    )
    tags = TaggableManager(related_name='tagged_services')

    csv_headers = [
        'code', 'name', 'manager', 'service_group', 'status', 'start_date', 'stop_date', 
        'level_important', 'tenant', 'backup_type', 'enduser', 'customer', 'user_influence', 
        'kpi', 'verify', 'monitored', 'dev_unit', 'dedicated', 'monitored_off_time','slug'
    ]

    def get_absolute_url(self):
        return reverse('service:service', args=[self.slug])
    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class ServiceUser(ChangeLoggedModel):
    """
    A Service User represents a Operating System User in an Instance.
    """
    service = models.ForeignKey(
        to='Service',
        on_delete=models.PROTECT,
        related_name='service_users'
    )
    instance = models.ForeignKey(
        to='dcim.Instance',
        on_delete=models.PROTECT,
        related_name='service_users'
    )
    username = models.CharField(
        max_length=500
    )
    description = models.CharField(
        max_length=500,
        blank=True
    )
    note = models.CharField(
        max_length=500,
        blank=True
    )


class ServiceContact(ChangeLoggedModel):
    service = models.ForeignKey(
        to='Service',
        on_delete=models.PROTECT,
        related_name='service_contacts'
    )
    name = models.CharField(
        max_length=255
    )
    email = models.EmailField(
        max_length=255
    )
    phone = models.CharField(
        max_length=255
    )
    address = models.CharField(
        max_length=255
    )
    description = models.CharField(
        max_length=255
    )
    note = models.CharField(
        max_length=500
    )


class ServiceManager(ChangeLoggedModel):
    service = models.ForeignKey(
        to='Service',
        on_delete=models.PROTECT,
        related_name='service_managers'
    )
    manager = models.ForeignKey(
        to='auth.User',
        on_delete=models.PROTECT,
        related_name='service_managers'
    )
    responsible = models.PositiveSmallIntegerField(
        choices=SERVICE_MANAGER_RESPONSIBLE_CHOICES
    )
    description = models.CharField(
        max_length=255
    )
    note = models.CharField(
        max_length=500
    )


class ServiceDocument(ChangeLoggedModel):
    service = models.ForeignKey(
        to='Service',
        on_delete=models.PROTECT,
        related_name='service_documents'
    )
    name = models.CharField(
        max_length=255
    )
    attach_file = models.CharField(
        max_length=255
    )
    path = models.CharField(
        max_length=500
    )
    file_name = models.CharField(
        max_length=500
    )
    description = models.CharField(
        max_length=255
    )
    note = models.CharField(
        max_length=500
    )


class ServiceLicense(ChangeLoggedModel):
    service = models.ForeignKey(
        to='Service',
        on_delete=models.PROTECT,
        related_name='service_licenses'
    )
    license = models.CharField(
        max_length=255
    )
    # TODO: Fix type field choice list
    type = models.PositiveSmallIntegerField(
        choices=SERVICE_LICENSE_TYPE_CHOICES,
        help_text='Service License Type: ' + convert_choices_to_str(SERVICE_LICENSE_TYPE_CHOICES)
    )
    value = models.CharField(
        max_length=255
    )
    start_time = models.DateTimeField()
    expire_time = models.DateTimeField()
    alarm_time = models.DateTimeField()
    description = models.CharField(
        max_length=255
    )
    note = models.CharField(
        max_length=500
    )

# TODO: conghm1 - Implement Database table, Module tables before implement following models

#
# class ServiceDatabase(ChangeLoggedModel):
#     """
#     A Service Database represents  a database used by a service.
#     """
#
#     service = models.ForeignKey(
#         to='Service',
#         on_delete=models.PROTECT,
#         related_name='service_databases'
#     )
#     # TODO: missing db_id field
#     username = models.CharField(
#         max_length=255,
#     )
#     # TODO: missing status field
#     description = models.CharField(
#         max_length=255,
#     )
#     note = models.CharField(
#         max_length=500,
#     )
#
#
# class ServiceDatabaseTable(ChangeLoggedModel):
#
#     database = models.ForeignKey(
#         to='ServiceDatabase',
#         on_delete=models.PROTECT,
#         related_name='service_database_tables'
#     )
#     table_name = models.CharField(
#         max_length=255,
#     )
#     type = models.PositiveSmallIntegerField(
#         choices=SERVICE_DATABASE_TABLE_TYPE_CHOICES
#     )
#     owner = models.CharField(
#         max_length=255,
#     )
#     # TODO: missing status field
#     # TODO: missing dedicated field
#     description = models.CharField(
#         max_length=255,
#     )
#     note = models.CharField(
#         max_length=500,
#     )
#
#
# class ServiceParameter(ChangeLoggedModel):
#     """
#     A Service Parameter represents  a parameter - properties used by a service.
#     """
#
#     service = models.ForeignKey(
#         to='Service',
#         on_delete=models.PROTECT,
#         related_name='service_parameters'
#     )
#     # TODO: missing module_id field
#     database = models.ForeignKey(
#         to='ServiceDatabase',
#         on_delete=models.PROTECT,
#         related_name='service_parameters',
#         blank=True,
#         null=True
#     )
#     query = models.CharField(
#         max_length=500,
#     )
#     # TODO: missing status field
#     description = models.CharField(
#         max_length=1000,
#     )
#
#
# class ServiceParameterDetail(ChangeLoggedModel):
#     parameter = models.ForeignKey(
#         to='Service',
#         on_delete=models.PROTECT,
#         related_name='service_parameter_details'
#     )
#     key = models.CharField(
#         max_length=500,
#     )
#     value = models.CharField(
#         max_length=500,
#     )
#     filter = models.CharField(
#         max_length=500,
#     )
#     desc = models.CharField(
#         max_length=500,
#     )


class ModuleGroup(ChangeLoggedModel):
    code = models.CharField(
        max_length=255,
        unique=True
    )
    name = models.CharField(
        max_length=255
    )
    status = models.PositiveSmallIntegerField(
        choices=MODULE_GROUP_STATUS_CHOICES,
        help_text='Module Group Status: ' + convert_choices_to_str(MODULE_GROUP_STATUS_CHOICES)
    )
    description = models.CharField(
        max_length=255
    )
    slug = models.SlugField(
        unique=True
    )
    tags = TaggableManager()


class Module(ChangeLoggedModel):
    service = models.ForeignKey(
        to='Service',
        on_delete=models.PROTECT,
        related_name='modules'
    )
    group = models.ForeignKey(
        to='ModuleGroup',
        on_delete=models.PROTECT,
        related_name='modules',
        blank=True,
        null=True
    )
    code = models.CharField(
        max_length=255,
        unique=True
    )
    name = models.CharField(
        max_length=255
    )
    type = models.PositiveSmallIntegerField(
        choices=MODULE_TYPE_CHOICES,
        help_text='Module Type: ' + convert_choices_to_str(MODULE_TYPE_CHOICES)
    )
    service_user = models.OneToOneField(
        to='ServiceUser',
        on_delete=models.PROTECT,
        related_name='module',
    )
    module_function = models.CharField(
        max_length=500,
    )
    pro_language = models.PositiveSmallIntegerField(
        choices=MODULE_PROGRAMMING_LANGUAGE_CHOICES,
        help_text='Module Programming Language: ' + convert_choices_to_str(MODULE_PROGRAMMING_LANGUAGE_CHOICES)
    )
    web_server = models.PositiveSmallIntegerField(
        choices=MODULE_WEB_SERVER_CHOICES,
        help_text='Module Web Server: ' + convert_choices_to_str(MODULE_WEB_SERVER_CHOICES)
    )
    status = models.PositiveSmallIntegerField(
        choices=MODULE_STATUS_CHOICES,
        help_text='Module Status: ' + convert_choices_to_str(MODULE_STATUS_CHOICES)
    )
    real_status = models.PositiveSmallIntegerField(
        choices=MODULE_REAL_STATUS_CHOICES,
        help_text='Module Real Status: ' + convert_choices_to_str(MODULE_REAL_STATUS_CHOICES)
    )
    port_mm = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(65535)],
        verbose_name='Port number',
        null=True,
    )
    uptime = models.CharField(
        max_length=500
    )
    purpose = models.CharField(
        max_length=500
    )
    description = models.CharField(
        max_length=500,
        blank=True
    )
    stop_date = models.DateTimeField(
        null=True
    )
    reason_deactive = models.CharField(
        max_length=500,
        blank=True
    )
    slug = models.SlugField(
        unique=True
    )
    tags = TaggableManager()


class ModuleUrl(ChangeLoggedModel):
    module = models.ForeignKey(
        to='Module',
        on_delete=models.PROTECT,
        related_name='module_urls'
    )
    url = models.URLField(
        max_length=255
    )
    type = models.PositiveSmallIntegerField(
        choices=MODULE_URL_TYPE_CHOICES,
        help_text='Module URL Type' + convert_choices_to_str(MODULE_URL_TYPE_CHOICES),
        default=MODULE_URL_TYPE_URL
    )
    is_lb_url = models.BooleanField(
        default=False,
        help_text="Determine this module URL is LB URL or not"
    )
    description = models.CharField(
        max_length=500
    )
    note = models.CharField(
        max_length=2000
    )
