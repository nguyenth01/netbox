import urllib.parse

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import Client, TestCase
from django.urls import reverse

from dcim.constants import CABLE_TYPE_CAT6, IFACE_FF_1GE_FIXED, INSTANCE_STATUS_PRODUCTION, INSTANCE_LEVEL_NORMAL
from dcim.models import (
    Cable, Device, DeviceRole, DeviceType, Interface, InventoryItem, Manufacturer, Platform, Rack, RackGroup,
    RackReservation, RackRole, Site, Region, VirtualChassis, InstanceRole, Instance, )
from tenancy.models import Tenant
from utilities.testing import SuperUserLoggedInTestCase


class RegionTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        # Create three Regions
        for i in range(1, 4):
            Region(name='Region {}'.format(i), slug='region-{}'.format(i)).save()

    def test_region_list(self):

        url = reverse('dcim:region_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class SiteTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        region = Region(name='Region 1', slug='region-1')
        region.save()

        Site.objects.bulk_create([
            Site(name='Site 1', slug='site-1', region=region),
            Site(name='Site 2', slug='site-2', region=region),
            Site(name='Site 3', slug='site-3', region=region),
        ])

    def test_site_list(self):

        url = reverse('dcim:site_list')
        params = {
            "region": Region.objects.first().slug,
        }

        response = self.client.get('{}?{}'.format(url, urllib.parse.urlencode(params)))
        self.assertEqual(response.status_code, 200)

    def test_site(self):

        site = Site.objects.first()
        response = self.client.get(site.get_absolute_url())
        self.assertEqual(response.status_code, 200)


class RackGroupTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        site = Site(name='Site 1', slug='site-1')
        site.save()

        RackGroup.objects.bulk_create([
            RackGroup(name='Rack Group 1', slug='rack-group-1', site=site),
            RackGroup(name='Rack Group 2', slug='rack-group-2', site=site),
            RackGroup(name='Rack Group 3', slug='rack-group-3', site=site),
        ])

    def test_rackgroup_list(self):

        url = reverse('dcim:rackgroup_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class RackTypeTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        RackRole.objects.bulk_create([
            RackRole(name='Rack Role 1', slug='rack-role-1'),
            RackRole(name='Rack Role 2', slug='rack-role-2'),
            RackRole(name='Rack Role 3', slug='rack-role-3'),
        ])

    def test_rackrole_list(self):

        url = reverse('dcim:rackrole_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class RackReservationTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        User = get_user_model()
        user = User(username='testuser', email='testuser@example.com')
        user.save()

        site = Site(name='Site 1', slug='site-1')
        site.save()

        rack = Rack(name='Rack 1', site=site)
        rack.save()

        RackReservation.objects.bulk_create([
            RackReservation(rack=rack, user=user, units=[1, 2, 3], description='Reservation 1'),
            RackReservation(rack=rack, user=user, units=[4, 5, 6], description='Reservation 2'),
            RackReservation(rack=rack, user=user, units=[7, 8, 9], description='Reservation 3'),
        ])

    def test_rackreservation_list(self):

        url = reverse('dcim:rackreservation_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class RackTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        site = Site(name='Site 1', slug='site-1')
        site.save()

        Rack.objects.bulk_create([
            Rack(name='Rack 1', site=site),
            Rack(name='Rack 2', site=site),
            Rack(name='Rack 3', site=site),
        ])

    def test_rack_list(self):

        url = reverse('dcim:rack_list')
        params = {
            "site": Site.objects.first().slug,
        }

        response = self.client.get('{}?{}'.format(url, urllib.parse.urlencode(params)))
        self.assertEqual(response.status_code, 200)

    def test_rack(self):

        rack = Rack.objects.first()
        response = self.client.get(rack.get_absolute_url())
        self.assertEqual(response.status_code, 200)


class ManufacturerTypeTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        Manufacturer.objects.bulk_create([
            Manufacturer(name='Manufacturer 1', slug='manufacturer-1'),
            Manufacturer(name='Manufacturer 2', slug='manufacturer-2'),
            Manufacturer(name='Manufacturer 3', slug='manufacturer-3'),
        ])

    def test_manufacturer_list(self):

        url = reverse('dcim:manufacturer_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class DeviceTypeTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        manufacturer = Manufacturer(name='Manufacturer 1', slug='manufacturer-1')
        manufacturer.save()

        DeviceType.objects.bulk_create([
            DeviceType(model='Device Type 1', slug='device-type-1', manufacturer=manufacturer),
            DeviceType(model='Device Type 2', slug='device-type-2', manufacturer=manufacturer),
            DeviceType(model='Device Type 3', slug='device-type-3', manufacturer=manufacturer),
        ])

    def test_devicetype_list(self):

        url = reverse('dcim:devicetype_list')
        params = {
            "manufacturer": Manufacturer.objects.first().slug,
        }

        response = self.client.get('{}?{}'.format(url, urllib.parse.urlencode(params)))
        self.assertEqual(response.status_code, 200)

    def test_devicetype(self):

        devicetype = DeviceType.objects.first()
        response = self.client.get(devicetype.get_absolute_url())
        self.assertEqual(response.status_code, 200)


class DeviceRoleTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        DeviceRole.objects.bulk_create([
            DeviceRole(name='Device Role 1', slug='device-role-1'),
            DeviceRole(name='Device Role 2', slug='device-role-2'),
            DeviceRole(name='Device Role 3', slug='device-role-3'),
        ])

    def test_devicerole_list(self):

        url = reverse('dcim:devicerole_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class PlatformTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        Platform.objects.bulk_create([
            Platform(name='Platform 1', slug='platform-1'),
            Platform(name='Platform 2', slug='platform-2'),
            Platform(name='Platform 3', slug='platform-3'),
        ])

    def test_platform_list(self):

        url = reverse('dcim:platform_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


class DeviceTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        site = Site(name='Site 1', slug='site-1')
        site.save()

        manufacturer = Manufacturer(name='Manufacturer 1', slug='manufacturer-1')
        manufacturer.save()

        devicetype = DeviceType(model='Device Type 1', manufacturer=manufacturer)
        devicetype.save()

        devicerole = DeviceRole(name='Device Role 1', slug='device-role-1')
        devicerole.save()

        manager = User(username='manager1',password='testpassword')
        manager.save()

        Device.objects.bulk_create([
            Device(name='Device 1', site=site, device_type=devicetype, device_role=devicerole, manager=manager),
            Device(name='Device 2', site=site, device_type=devicetype, device_role=devicerole, manager=manager),
            Device(name='Device 3', site=site, device_type=devicetype, device_role=devicerole, manager=manager),
        ])

    def test_device_list(self):

        url = reverse('dcim:device_list')
        params = {
            "device_type_id": DeviceType.objects.first().pk,
            "role": DeviceRole.objects.first().slug,
        }

        response = self.client.get('{}?{}'.format(url, urllib.parse.urlencode(params)))
        self.assertEqual(response.status_code, 200)

    def test_device(self):

        device = Device.objects.first()
        response = self.client.get(device.get_absolute_url())
        self.assertEqual(response.status_code, 200)


class InventoryItemTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        site = Site(name='Site 1', slug='site-1')
        site.save()

        manufacturer = Manufacturer(name='Manufacturer 1', slug='manufacturer-1')
        manufacturer.save()

        devicetype = DeviceType(model='Device Type 1', manufacturer=manufacturer)
        devicetype.save()

        devicerole = DeviceRole(name='Device Role 1', slug='device-role-1')
        devicerole.save()

        manager = User(username='manager1',password='testpassword')
        manager.save()

        device = Device(name='Device 1', site=site, device_type=devicetype, device_role=devicerole, manager=manager)
        device.save()

        InventoryItem.objects.bulk_create([
            InventoryItem(device=device, name='Inventory Item 1'),
            InventoryItem(device=device, name='Inventory Item 2'),
            InventoryItem(device=device, name='Inventory Item 3'),
        ])

    def test_inventoryitem_list(self):

        url = reverse('dcim:inventoryitem_list')
        params = {
            "device_id": Device.objects.first().pk,
        }

        response = self.client.get('{}?{}'.format(url, urllib.parse.urlencode(params)))
        self.assertEqual(response.status_code, 200)

    def test_inventoryitem(self):

        inventoryitem = InventoryItem.objects.first()
        response = self.client.get(inventoryitem.get_absolute_url())
        self.assertEqual(response.status_code, 200)


class CableTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        site = Site(name='Site 1', slug='site-1')
        site.save()

        manufacturer = Manufacturer(name='Manufacturer 1', slug='manufacturer-1')
        manufacturer.save()

        devicetype = DeviceType(model='Device Type 1', manufacturer=manufacturer)
        devicetype.save()

        devicerole = DeviceRole(name='Device Role 1', slug='device-role-1')
        devicerole.save()

        manager = User(username='manager1', password='testpassword')
        manager.save()

        device1 = Device(name='Device 1', site=site, device_type=devicetype, device_role=devicerole, manager=manager)
        device1.save()
        device2 = Device(name='Device 2', site=site, device_type=devicetype, device_role=devicerole, manager=manager)
        device2.save()

        iface1 = Interface(device=device1, name='Interface 1', form_factor=IFACE_FF_1GE_FIXED)
        iface1.save()
        iface2 = Interface(device=device1, name='Interface 2', form_factor=IFACE_FF_1GE_FIXED)
        iface2.save()
        iface3 = Interface(device=device1, name='Interface 3', form_factor=IFACE_FF_1GE_FIXED)
        iface3.save()
        iface4 = Interface(device=device2, name='Interface 1', form_factor=IFACE_FF_1GE_FIXED)
        iface4.save()
        iface5 = Interface(device=device2, name='Interface 2', form_factor=IFACE_FF_1GE_FIXED)
        iface5.save()
        iface6 = Interface(device=device2, name='Interface 3', form_factor=IFACE_FF_1GE_FIXED)
        iface6.save()

        Cable(termination_a=iface1, termination_b=iface4, type=CABLE_TYPE_CAT6).save()
        Cable(termination_a=iface2, termination_b=iface5, type=CABLE_TYPE_CAT6).save()
        Cable(termination_a=iface3, termination_b=iface6, type=CABLE_TYPE_CAT6).save()

    def test_cable_list(self):

        url = reverse('dcim:cable_list')
        params = {
            "type": CABLE_TYPE_CAT6,
        }

        response = self.client.get('{}?{}'.format(url, urllib.parse.urlencode(params)))
        self.assertEqual(response.status_code, 200)

    def test_cable(self):

        cable = Cable.objects.first()
        response = self.client.get(cable.get_absolute_url())
        self.assertEqual(response.status_code, 200)


class VirtualMachineTestCase(TestCase):

    def setUp(self):

        self.client = Client()

        site = Site.objects.create(name='Site 1', slug='site-1')
        manufacturer = Manufacturer.objects.create(name='Manufacturer', slug='manufacturer-1')
        device_type = DeviceType.objects.create(
            manufacturer=manufacturer, model='Device Type 1', slug='device-type-1'
        )
        device_role = DeviceRole.objects.create(
            name='Device Role', slug='device-role-1'
        )
        manager = User.objects.create(username='manager1')

        # Create 9 member Devices
        device1 = Device.objects.create(
            device_type=device_type, device_role=device_role, name='Device 1', site=site, manager=manager
        )
        device2 = Device.objects.create(
            device_type=device_type, device_role=device_role, name='Device 2', site=site, manager=manager
        )
        device3 = Device.objects.create(
            device_type=device_type, device_role=device_role, name='Device 3', site=site, manager=manager
        )
        device4 = Device.objects.create(
            device_type=device_type, device_role=device_role, name='Device 4', site=site, manager=manager
        )
        device5 = Device.objects.create(
            device_type=device_type, device_role=device_role, name='Device 5', site=site, manager=manager
        )
        device6 = Device.objects.create(
            device_type=device_type, device_role=device_role, name='Device 6', site=site, manager=manager
        )

        # Create three VirtualChassis with two members each
        vc1 = VirtualChassis.objects.create(master=device1, domain='test-domain-1')
        Device.objects.filter(pk=device2.pk).update(virtual_chassis=vc1, vc_position=2)
        vc2 = VirtualChassis.objects.create(master=device3, domain='test-domain-2')
        Device.objects.filter(pk=device4.pk).update(virtual_chassis=vc2, vc_position=2)
        vc3 = VirtualChassis.objects.create(master=device5, domain='test-domain-3')
        Device.objects.filter(pk=device6.pk).update(virtual_chassis=vc3, vc_position=2)

    def test_virtualchassis_list(self):

        url = reverse('dcim:virtualchassis_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_virtualchassis(self):

        virtualchassis = VirtualChassis.objects.first()
        response = self.client.get(virtualchassis.get_absolute_url())
        self.assertEqual(response.status_code, 200)


class InstanceRoleTestCase(SuperUserLoggedInTestCase):

    def setUp(self):

        super().setUp()

        self.instancerole1 = InstanceRole.objects.create(name='Test Instance Role 1', slug='test-instance-role-1', color='00ff00')
        self.instancerole2 = InstanceRole.objects.create(name='Test Instance Role 2', slug='test-instance-role-2', color='ff00ff')
        self.instancerole3 = InstanceRole.objects.create(name='Test Instance Role 3', slug='test-instance-role-3', color='0000ff')
        self.instancerole4 = InstanceRole.objects.create(name='Test Instance Role 4', slug='test-instance-role-4', color='fff0ff')

        self.site = Site.objects.create(name='Site 1', slug='site-1')
        self.manufacturer = Manufacturer.objects.create(name='Manufacturer', slug='manufacturer-1')
        self.device_type = DeviceType.objects.create(manufacturer=self.manufacturer, model='Device Type 1', slug='device-type-1')
        self.device_role = DeviceRole.objects.create(name='Device Role', slug='device-role-1')
        self.manager = User.objects.create(username='manager1', password='testpassword')

        self.device1 = Device.objects.create(
            device_type=self.device_type, device_role=self.device_role, name='Device 1', site=self.site, manager=self.manager
        )

        self.instance1 = Instance.objects.create(name='Instance 1', device=self.device1, instance_role=self.instancerole1, manager=self.manager)
        self.instance2 = Instance.objects.create(name='Instance 2', device=self.device1, instance_role=self.instancerole1, manager=self.manager)
        self.instance3 = Instance.objects.create(name='Instance 3', device=self.device1, instance_role=self.instancerole1, manager=self.manager)

        self.instance4 = Instance.objects.create(name='Instance 4', parent_instance=self.instance1, instance_role=self.instancerole1, manager=self.manager)
        self.instance5 = Instance.objects.create(name='Instance 5', parent_instance=self.instance2, instance_role=self.instancerole2, manager=self.manager)
        self.instance6 = Instance.objects.create(name='Instance 6', parent_instance=self.instance3, instance_role=self.instancerole2, manager=self.manager)

    def test_instancerole_list(self):

        url = reverse('dcim:instancerole_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table'].data), 4)

    def test_instancerole_get_add_view(self):

        url = reverse('dcim:instancerole_add')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['form'].fields), 3)
        self.assertEqual(response.context['form'].fields['name'].label, 'Name')
        self.assertEqual(response.context['form'].fields['slug'].label, 'Slug')
        self.assertEqual(response.context['form'].fields['color'].label, 'Color')

    def test_instancerole_add_valid(self):

        data = {
            'name': 'Test Instance Role X',
            'slug': 'test-instance-role-x',
            'color': 'cc3399',
        }

        url = reverse('dcim:instancerole_add')
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

        url = reverse('dcim:instancerole_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table'].data), 5)

    def test_instancerole_add_invalid_data(self):

        instancerole_form_data = {
            'name': 'Test Instance Role 1',
            'slug': 'test-instance-role-1',
            'color': 'cc3399',
        }

        url = reverse('dcim:instancerole_add')
        response = self.client.post(url, data=instancerole_form_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].is_valid(), False)
        self.assertGreater(len(response.context['form'].errors['name']), 0)
        self.assertGreater(len(response.context['form'].errors['slug']), 0)

    def test_instancerole_get_edit_view(self):

        url = reverse('dcim:instancerole_edit', kwargs={'slug': self.instancerole1.slug})

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['form'].fields), 3)
        self.assertEqual(response.context['form'].initial['name'], self.instancerole1.name)
        self.assertEqual(response.context['form'].initial['slug'], self.instancerole1.slug)
        self.assertEqual(response.context['form'].initial['color'], self.instancerole1.color)

    def test_instancerole_edit_valid(self):

        data = {
            'name': 'Test Instance Role x',
            'slug': 'test-instance-role-x',
            'color': 'cc3399',
        }

        url = reverse('dcim:instancerole_edit', kwargs={'slug': self.instancerole1.slug})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_instancerole_edit_valid_change_color(self):

        data = {
            'name': 'Test Instance Role 1',
            'slug': 'test-instance-role-1',
            'color': 'cc3399',
        }

        url = reverse('dcim:instancerole_edit', kwargs={'slug': self.instancerole1.slug})
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_instancerole_edit_invalid_data(self):

        data = {
            'name': 'Test Instance Role 2',
            'slug': 'test-instance-role-2',
            'color': 'cc3399',
        }

        url = reverse('dcim:instancerole_add')
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].is_valid(), False)
        self.assertGreater(len(response.context['form'].errors['name']), 0)
        self.assertGreater(len(response.context['form'].errors['slug']), 0)

    def test_instancerole_export_data(self):

        url = reverse('dcim:instancerole_list')
        response = self.client.get(url+'?export')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'text/csv')

        expected_response = b'name,slug,color,instance_count\n' +\
                            b'Test Instance Role 1,test-instance-role-1,00ff00,4\n' +\
                            b'Test Instance Role 2,test-instance-role-2,ff00ff,2\n' +\
                            b'Test Instance Role 3,test-instance-role-3,0000ff,0\n' + \
                            b'Test Instance Role 4,test-instance-role-4,fff0ff,0'
        self.assertEqual(response.content, expected_response)

    def test_instancerole_get_bulk_delete_view(self):

        data = {
            'pk': [self.instancerole3.pk, self.instancerole4.pk],
            'return_url': reverse('dcim:instancerole_list'),
        }
        url = reverse('dcim:instancerole_bulk_delete')
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table'].data), 2)
        self.assertEqual(response.context['table'].data[0].pk, data['pk'][0])
        self.assertEqual(response.context['table'].data[1].pk, data['pk'][1])

    def test_instancerole_bulk_delete(self):

        data = {
            'pk': [self.instancerole3.pk, self.instancerole4.pk],
            'return_url': reverse('dcim:instancerole_list'),
            '_confirm': '',
            'confirm': True
        }
        url = reverse('dcim:instancerole_bulk_delete')
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InstanceRole.objects.count(), 2)
        self.assertEqual(response.url, reverse('dcim:instancerole_list'))

    def test_instancerole_bulk_delete_invalid(self):

        data = {
            'pk': [self.instancerole1.pk, self.instancerole2.pk],
            'return_url': reverse('dcim:instancerole_list'),
            '_confirm': '',
            'confirm': True
        }
        url = reverse('dcim:instancerole_bulk_delete')
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(InstanceRole.objects.count(), 4)
        self.assertEqual(response.url, reverse('dcim:instancerole_list'))


class InstanceTestCase(SuperUserLoggedInTestCase):

    def setUp(self):

        super().setUp()

        self.site = Site.objects.create(name='Site 1', slug='site-1')
        self.tenant = Tenant.objects.create(name="Tenant 1", slug='tenant-1')
        self.manufacturer = Manufacturer.objects.create(name='Manufacturer', slug='manufacturer-1')
        self.device_type = DeviceType.objects.create(manufacturer=self.manufacturer, model='Device Type 1', slug='device-type-1')
        self.device_role = DeviceRole.objects.create(name='Device Role', slug='device-role-1')
        self.instancerole1 = InstanceRole.objects.create(name='Instance Role 1', slug='instance-role-1')
        self.instancerole2 = InstanceRole.objects.create(name='Instance Role 2', slug='instance-role-2')
        self.manager = User.objects.create(username='manager1', password='testpassword')

        self.device1 = Device.objects.create(
            device_type=self.device_type, device_role=self.device_role, name='Device 1', site=self.site, manager=self.manager
        )

        self.instance1 = Instance.objects.create(name='Instance 1', device=self.device1, instance_role=self.instancerole1, manager=self.manager, tenant=self.tenant)
        self.instance2 = Instance.objects.create(name='Instance 2', device=self.device1, instance_role=self.instancerole1, manager=self.manager, tenant=self.tenant)
        self.instance3 = Instance.objects.create(name='Instance 3', device=self.device1, instance_role=self.instancerole1, manager=self.manager, tenant=self.tenant)

        self.instance4 = Instance.objects.create(name='Instance 4', parent_instance=self.instance1, instance_role=self.instancerole2, manager=self.manager)
        self.instance5 = Instance.objects.create(name='Instance 5', parent_instance=self.instance1, instance_role=self.instancerole2, manager=self.manager)
        self.instance6 = Instance.objects.create(name='Instance 6', parent_instance=self.instance3, instance_role=self.instancerole2, manager=self.manager)

        self.iface1 = Interface(instance=self.instance1, name='Interface 1', form_factor=IFACE_FF_1GE_FIXED)
        self.iface1.save()
        self.iface2 = Interface(instance=self.instance1, name='Interface 2', form_factor=IFACE_FF_1GE_FIXED)
        self.iface2.save()
        self.iface3 = Interface(instance=self.instance1, name='Interface 3', form_factor=IFACE_FF_1GE_FIXED)
        self.iface3.save()
        self.iface4 = Interface(instance=self.instance2, name='Interface 1', form_factor=IFACE_FF_1GE_FIXED)
        self.iface4.save()
        self.iface5 = Interface(instance=self.instance2, name='Interface 2', form_factor=IFACE_FF_1GE_FIXED)
        self.iface5.save()
        self.iface6 = Interface(instance=self.instance3, name='Interface 3', form_factor=IFACE_FF_1GE_FIXED)
        self.iface6.save()

    def test_instance_list(self):

        url = reverse('dcim:instance_list')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table'].data), 6)

    def test_instance_list_filter_by_device_id(self):

        url = reverse('dcim:instance_list')

        response = self.client.get(url, {'device_id': self.device1.pk})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table'].data), 3)

    def test_instance_list_filter_by_parent_instance_id(self):

        url = reverse('dcim:instance_list')

        response = self.client.get(url, {'parent_instance_id': self.instance1.pk})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table'].data), 2)

    def test_instance_list_filter_by_tenant(self):

        url = reverse('dcim:instance_list')

        response = self.client.get(url, {'tenant': self.tenant.slug})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['table'].data), 3)

    def test_instance_create_valid(self):

        instance_form_data = {
            'name': 'New Instance 1',
            'device': self.device1.pk,
            'instance_role': self.instancerole1.pk,
            'manager': self.manager.pk,
            'status': INSTANCE_STATUS_PRODUCTION,
            'level_important': INSTANCE_LEVEL_NORMAL,
            'local_context_data': '',
        }

        instance_add_url = reverse('dcim:instance_add')
        instance_add_response = self.client.post(instance_add_url, data=instance_form_data)
        self.assertEqual(instance_add_response.status_code, 302)

        instance_list_url = reverse('dcim:instance_list')
        instance_list_response = self.client.get(instance_list_url)
        self.assertEqual(instance_list_response.status_code, 200)
        self.assertEqual(len(instance_list_response.context['table'].data), 7)

    def test_instance_create_invalid_name(self):

        instance_form_data = {
            'name': 'Instance 1',
            'device': self.device1.pk,
            'instance_role': self.instancerole1.pk,
            'manager': self.manager.pk,
            'status': INSTANCE_STATUS_PRODUCTION,
            'level_important': INSTANCE_LEVEL_NORMAL,
            'local_context_data': '',
        }

        instance_add_url = reverse('dcim:instance_add')
        instance_add_response = self.client.post(instance_add_url, data=instance_form_data)
        self.assertEqual(instance_add_response.status_code, 200)
        self.assertEqual(instance_add_response.context['form'].is_valid(), False)
        self.assertGreater(len(instance_add_response.context['form'].errors['name']), 0)

    def test_instance_create_invalid_null_device_parent_instance(self):

        instance_form_data = {
            'name': 'New Instance 1',
            'instance_role': self.instancerole1.pk,
            'manager': self.manager.pk,
            'status': INSTANCE_STATUS_PRODUCTION,
            'level_important': INSTANCE_LEVEL_NORMAL,
            'local_context_data': '',
        }

        instance_add_url = reverse('dcim:instance_add')
        instance_add_response = self.client.post(instance_add_url, data=instance_form_data)
        self.assertEqual(instance_add_response.status_code, 200)
        self.assertEqual(instance_add_response.context['form'].is_valid(), False)
        self.assertGreater(len(instance_add_response.context['form'].errors['device']), 0)
        self.assertGreater(len(instance_add_response.context['form'].errors['parent_instance']), 0)

    def test_instance_create_invalid_set_both_device_parent_instance(self):

        instance_form_data = {
            'name': 'New Instance 1',
            'device': self.device1.pk,
            'parent_instance': self.instance1.pk,
            'instance_role': self.instancerole1.pk,
            'manager': self.manager.pk,
            'status': INSTANCE_STATUS_PRODUCTION,
            'level_important': INSTANCE_LEVEL_NORMAL,
            'local_context_data': '',
        }

        instance_add_url = reverse('dcim:instance_add')
        instance_add_response = self.client.post(instance_add_url, data=instance_form_data)
        self.assertEqual(instance_add_response.status_code, 200)
        self.assertEqual(instance_add_response.context['form'].is_valid(), False)
        self.assertGreater(len(instance_add_response.context['form'].errors['device']), 0)
        self.assertGreater(len(instance_add_response.context['form'].errors['parent_instance']), 0)

    def test_instance_detail(self):

        response = self.client.get(self.instance1.get_absolute_url())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['instance'].pk, self.instance1.pk)
        self.assertEqual(len(response.context['interfaces']), 3)
        self.assertEqual(len(response.context['related_instances']), 2)
